package reconn_bot

import (
	"log"
	"net/http"
	"ps2tg/proto"
	"sync"

	"github.com/PaulSonOfLars/gotgbot/v2"
)

type recon_bot struct {
	mtx     sync.Mutex
	current *gotgbot.Bot
	token   string
}

func New(token string) proto.IReconnBot {
	return &recon_bot{token: token}
}

func (self *recon_bot) Get() (*gotgbot.Bot, error) {
	if self.current != nil {
		return self.current, nil
	}
	err := self.Reconnect()
	return self.current, err
}

func (self *recon_bot) Reconnect() error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if self.current != nil {
		self.current.Close(nil)
	}
	bot, err := gotgbot.NewBot(self.token, opts())
	if err != nil {
		log.Printf("failed to create new bot: %v", err)
		return err
	}
	self.current = bot
	return nil
}

func opts() *gotgbot.BotOpts {
	return &gotgbot.BotOpts{
		BotClient: &gotgbot.BaseBotClient{
			Client: http.Client{},
			DefaultRequestOpts: &gotgbot.RequestOpts{
				Timeout: gotgbot.DefaultTimeout,
				APIURL:  gotgbot.DefaultAPIURL,
			},
		},
	}
}
