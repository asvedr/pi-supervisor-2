package sender

import (
	"ps2base/ps2types"
	"ps2tg/proto"
	"ps2tg/utils"
	"time"
)

type sender struct {
	bot         proto.IReconnBot
	chat_id     int64
	max_try     int
	max_msg_len int
}

func New(config *ps2types.Config, bot proto.IReconnBot) proto.ISender {
	return &sender{
		bot:         bot,
		chat_id:     *config.Tg.AdminId,
		max_try:     config.Tg.MaxTry,
		max_msg_len: config.Tg.MaxMsgLen,
	}
}

func (self *sender) SendMsg(msg string) {
	chunks := utils.SplitToChunks(msg, self.max_msg_len)
	for _, chunk := range chunks {
		sent := self.send(chunk)
		if !sent {
			return
		}
	}
}

func (self *sender) send(msg string) bool {
	for i := 0; i < self.max_try; i += 1 {
		err := self.send_msg(msg)
		if err == nil {
			return true
		}
		time.Sleep(time.Second)
		self.bot.Reconnect()
	}
	return false
}

func (self *sender) send_msg(msg string) error {
	bot, err := self.bot.Get()
	if err != nil {
		return err
	}
	_, err = bot.SendMessage(self.chat_id, msg, nil)
	return err
}
