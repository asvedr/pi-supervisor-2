module ps2tg

go 1.21.3

require (
	github.com/PaulSonOfLars/gotgbot/v2 v2.0.0-rc.22
	gitlab.com/asvedr/giter v1.3.0
)

require gitlab.com/asvedr/cldi v0.4.0
