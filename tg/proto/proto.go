package proto

import "github.com/PaulSonOfLars/gotgbot/v2"

type IReconnBot interface {
	Get() (*gotgbot.Bot, error)
	Reconnect() error
}

type ISender interface {
	SendMsg(msg string)
}

type IProcess interface {
	Run()
}

type ICommandHandler interface {
	Command() string
	Help() string
	Handle(text string) (string, error)
}
