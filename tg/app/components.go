package app

import (
	"ps2base/ps2client"
	"ps2base/ps2config_parser"
	"ps2base/ps2proto"
	"ps2base/ps2status_transition_observer"
	"ps2base/ps2types"
	"ps2tg/components/reconn_bot"
	"ps2tg/components/sender"
	"ps2tg/proto"
	"time"

	"gitlab.com/asvedr/cldi/di"
)

var Config = di.Singleton[*ps2types.Config]{
	ErrFunc: func() (*ps2types.Config, error) {
		prs := ps2config_parser.New()
		path := prs.RevealPath()
		return prs.Parse(path)
	},
}

var Client = di.Singleton[ps2proto.IClient]{
	PureFunc: func() ps2proto.IClient {
		return ps2client.New(Config.Get())
	},
}

var Observer = di.Singleton[ps2proto.IClientStatusTransitionObserver]{
	PureFunc: func() ps2proto.IClientStatusTransitionObserver {
		return ps2status_transition_observer.New(
			Client.Get(),
			time.Second*5,
			time.Millisecond*200,
		)
	},
}

var Sender = di.Singleton[proto.ISender]{
	PureFunc: func() proto.ISender {
		cnf := Config.Get()
		bot := reconn_bot.New(cnf.Tg.Token)
		return sender.New(cnf, bot)
	},
}
