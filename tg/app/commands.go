package app

import (
	"ps2tg/commands/get_alive"
	"ps2tg/commands/get_info"
	"ps2tg/commands/get_logs"
	"ps2tg/commands/get_statuses"
	"ps2tg/commands/get_sys_info"
	"ps2tg/commands/get_version"
	"ps2tg/commands/help"
	"ps2tg/commands/restart"
	"ps2tg/commands/start"
	"ps2tg/commands/stop"
	"ps2tg/proto"

	"gitlab.com/asvedr/cldi/di"
)

var Commands = di.Singleton[[]proto.ICommandHandler]{
	PureFunc: func() []proto.ICommandHandler {
		return help.WithHelp(
			Sender.Get(),
			get_version.New(),
			get_sys_info.New(Client.Get()),
			start.New(Client.Get(), Observer.Get(), Sender.Get()),
			stop.New(Client.Get(), Observer.Get(), Sender.Get()),
			restart.New(Client.Get()),
			get_alive.New(Client.Get()),
			get_info.New(Client.Get()),
			get_logs.New(Client.Get()),
			get_statuses.New(Client.Get()),
		)
	},
}
