package app

import (
	"ps2tg/components/reconn_bot"
	"ps2tg/processes/alert_notifier"
	"ps2tg/processes/ui"
	"ps2tg/proto"

	"gitlab.com/asvedr/cldi/di"
)

var ProcAlert = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		return alert_notifier.New(
			Config.Get(),
			Sender.Get(),
			Client.Get(),
		)
	},
}

var ProcUi = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		cnf := Config.Get()
		return ui.New(
			cnf,
			reconn_bot.New(cnf.Tg.Token),
			Commands.Get(),
		)
	},
}
