package alert_notifier

import (
	"fmt"
	"ps2base/ps2proto"
	"ps2base/ps2types"
	"ps2tg/proto"
	"time"
)

const alerts_per_request int = 10
const sleep_per_alert = time.Millisecond * 100

var alert_type_str = map[ps2types.AlertType]string{
	ps2types.AlertTypeFail:  "🔴",
	ps2types.AlertTypeOk:    "🟢",
	ps2types.AlertTypeNotif: "🔵",
}

type proc struct {
	config *ps2types.Config
	sender proto.ISender
	client ps2proto.IClient
}

func New(
	config *ps2types.Config,
	sender proto.ISender,
	client ps2proto.IClient,
) proto.IProcess {
	return &proc{
		config: config,
		sender: sender,
		client: client,
	}
}

func (self *proc) Run() {
	for {
		self.Step()
		time.Sleep(self.config.Tg.CheckAlertsTimeout)
	}
}

func (self *proc) Step() {
	alerts, err := self.client.GetAlerts(alerts_per_request)
	var msg string
	if err != nil {
		msg = "can not fetch alerts: " + err.Error()
		self.sender.SendMsg(msg)
		return
	}
	for _, alert := range alerts {
		msg = fmt.Sprintf(
			"%s %s: %s",
			alert_type_str[alert.Type],
			alert.Name,
			alert.Msg,
		)
		self.sender.SendMsg(msg)
		time.Sleep(sleep_per_alert)
	}
}
