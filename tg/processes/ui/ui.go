package ui

import (
	"errors"
	"fmt"
	"log"
	"ps2base/ps2types"
	"ps2tg/proto"
	"ps2tg/utils"
	"strings"
	"time"

	"github.com/PaulSonOfLars/gotgbot/v2"
	"github.com/PaulSonOfLars/gotgbot/v2/ext"
	"github.com/PaulSonOfLars/gotgbot/v2/ext/handlers"
	"github.com/PaulSonOfLars/gotgbot/v2/ext/handlers/filters/message"
)

type ui struct {
	config   *ps2types.Config
	bot      proto.IReconnBot
	handlers []proto.ICommandHandler
}

func New(
	config *ps2types.Config,
	bot proto.IReconnBot,
	handlers []proto.ICommandHandler,
) proto.IProcess {
	return &ui{config: config, bot: bot, handlers: handlers}
}

func (self *ui) Run() {
	for {
		self.bot.Reconnect()
		err := self.main_loop()
		log.Printf("TgUi error: %v", err)
		time.Sleep(time.Second)
	}
}

func (self *ui) main_loop() error {
	bot, err := self.bot.Get()
	if err != nil {
		return err
	}
	updater := ext.NewUpdater(&ext.UpdaterOpts{
		Dispatcher: ext.NewDispatcher(&ext.DispatcherOpts{
			// If an error is returned by a handler, log it and continue going.
			Error: func(b *gotgbot.Bot, ctx *ext.Context, err error) ext.DispatcherAction {
				log.Println("an error occurred while handling update:", err.Error())
				return ext.DispatcherActionNoop
			},
			MaxRoutines: ext.DefaultMaxRoutines,
		}),
	})
	dispatcher := updater.Dispatcher

	handler := func(bot *gotgbot.Bot, ctx *ext.Context) error {
		if ctx.EffectiveMessage.From.Id == *self.config.Tg.AdminId {
			return self.react_admin(bot, ctx)
		} else {
			return echo(bot, ctx)
		}
	}

	// Add echo handler to reply to all text messages.
	dispatcher.AddHandler(
		handlers.NewMessage(message.Text, handler),
	)
	err = updater.StartPolling(bot, &ext.PollingOpts{
		DropPendingUpdates: true,
		GetUpdatesOpts: &gotgbot.GetUpdatesOpts{
			Timeout: 9,
			RequestOpts: &gotgbot.RequestOpts{
				Timeout: time.Second * 10,
			},
		},
	})
	if err != nil {
		log.Printf("failed to start polling: %v", err.Error())
		return err
	}
	log.Printf("%s has been started...\n", bot.User.Username)

	// Idle, to keep updates coming in, and avoid bot stopping.
	updater.Idle()
	return errors.New("stop idling")
}

func echo(bot *gotgbot.Bot, ctx *ext.Context) error {
	info := fmt.Sprintf("tg_id: %d", ctx.EffectiveMessage.From.Id)
	_, err := ctx.EffectiveMessage.Reply(bot, info, nil)
	return err
}

func (self *ui) react_admin(bot *gotgbot.Bot, ctx *ext.Context) error {
	msg := ctx.EffectiveMessage.Text
	handler, msg := find_handler(self.handlers, msg)
	if handler == nil {
		return nil
	}
	resp, err := handler.Handle(msg)
	if err != nil {
		resp = "ERR: " + err.Error()
	}
	chunks := utils.SplitToChunks(resp, self.config.Tg.MaxMsgLen)
	for _, chunk := range chunks {
		_, err = ctx.EffectiveMessage.Reply(bot, chunk, nil)
		if err != nil {
			return err
		}
	}
	return nil
}

func find_handler(handlers []proto.ICommandHandler, msg string) (proto.ICommandHandler, string) {
	for _, handler := range handlers {
		if strings.HasPrefix(msg, handler.Command()) {
			trimmed := strings.TrimSpace(strings.TrimPrefix(msg, handler.Command()))
			return handler, trimmed
		}
	}
	return nil, ""
}
