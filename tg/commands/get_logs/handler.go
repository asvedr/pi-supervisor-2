package get_logs

import (
	"errors"
	"ps2base/ps2proto"
	"ps2tg/proto"
	"strconv"
	"strings"
)

type handler struct {
	client ps2proto.IClient
}

func New(
	client ps2proto.IClient,
) proto.ICommandHandler {
	return handler{client: client}
}

func (handler) Command() string {
	return "logs"
}

func (handler) Help() string {
	return "get logs of service"
}

func (self handler) Handle(text string) (string, error) {
	if len(text) == 0 {
		return "", errors.New("name is not set")
	}
	split := strings.Split(text, ",")
	name := split[0]
	count := 100
	var err error
	if len(split) > 1 {
		count, err = strconv.Atoi(split[1])
	}
	if err != nil {
		return "", err
	}
	return self.client.GetLogs(name, count)
}
