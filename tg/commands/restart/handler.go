package restart

import (
	"errors"
	"ps2base/ps2proto"
	"ps2tg/proto"
)

type handler struct {
	client ps2proto.IClient
}

func New(
	client ps2proto.IClient,
) proto.ICommandHandler {
	return handler{client: client}
}

func (handler) Command() string {
	return "restart"
}

func (handler) Help() string {
	return "restart service"
}

func (self handler) Handle(text string) (string, error) {
	if len(text) == 0 {
		return "", errors.New("name is not set")
	}
	err := self.client.Restart(text)
	if err != nil {
		return "", err
	}
	return "sent", err
}
