package get_info

import (
	"errors"
	"fmt"
	"ps2base/ps2proto"
	"ps2tg/proto"
)

type handler struct {
	client ps2proto.IClient
}

func New(
	client ps2proto.IClient,
) proto.ICommandHandler {
	return handler{client: client}
}

func (handler) Command() string {
	return "info"
}

func (handler) Help() string {
	return "info about single service"
}

func (self handler) Handle(text string) (string, error) {
	if len(text) == 0 {
		return "", errors.New("name is not set")
	}
	info, err := self.client.GetInfo(text)
	if err != nil {
		return "", err
	}
	msg := fmt.Sprintf(
		"mem: %s\nalive: %s\nlifetime: %s",
		info.MemUsed,
		info.Alive,
		info.Lifetime,
	)
	return msg, nil
}
