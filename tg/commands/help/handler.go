package help

import (
	"ps2tg/proto"
	"strings"

	"gitlab.com/asvedr/giter"
)

type handler struct {
	cmd      string
	handlers []proto.ICommandHandler
}

func WithHelp(
	sender proto.ISender,
	handlers ...proto.ICommandHandler,
) []proto.ICommandHandler {
	helpers := []proto.ICommandHandler{
		handler{cmd: "/help", handlers: handlers},
		handler{cmd: "help", handlers: handlers},
	}
	return append(handlers, helpers...)
}

func (self handler) Command() string {
	return self.cmd
}

func (handler) Help() string { return "" }

func (self handler) Handle(text string) (string, error) {
	lines := giter.Map(self.handlers, make_help)
	return strings.Join(lines, "\n"), nil
}

func make_help(h proto.ICommandHandler) string {
	return h.Command() + ": " + h.Help()
}
