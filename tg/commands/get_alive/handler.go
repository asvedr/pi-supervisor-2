package get_alive

import (
	"fmt"
	"ps2base/ps2proto"
	"ps2base/ps2types"
	"ps2tg/proto"
	"strings"

	"gitlab.com/asvedr/giter"
)

type handler struct {
	client ps2proto.IClient
}

func New(client ps2proto.IClient) proto.ICommandHandler {
	return handler{client: client}
}

func (handler) Command() string {
	return "alive"
}

func (handler) Help() string {
	return "get list of alive services"
}

func (self handler) Handle(text string) (string, error) {
	services, err := self.client.GetAlive()
	if err != nil {
		return "", err
	}
	f := func(s ps2types.ApiService) string {
		return fmt.Sprintf("%s:(%s)", s.Service, s.Mem)
	}
	msg := strings.Join(giter.Map(services, f), "\n")
	return msg, nil
}
