package get_version

import (
	"ps2base/ps2consts"
	"ps2tg/proto"
)

type handler struct{}

func New() proto.ICommandHandler {
	return handler{}
}

func (handler) Command() string {
	return "version"
}

func (handler) Help() string {
	return "get version"
}

func (self handler) Handle(text string) (string, error) {
	return ps2consts.Version, nil
}
