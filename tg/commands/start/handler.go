package start

import (
	"errors"
	"fmt"
	"ps2base/ps2proto"
	"ps2tg/proto"
)

type handler struct {
	client   ps2proto.IClient
	observer ps2proto.IClientStatusTransitionObserver
	sender   proto.ISender
}

func New(
	client ps2proto.IClient,
	observer ps2proto.IClientStatusTransitionObserver,
	sender proto.ISender,
) proto.ICommandHandler {
	return handler{
		client:   client,
		observer: observer,
		sender:   sender,
	}
}

func (handler) Command() string {
	return "start"
}

func (handler) Help() string {
	return "start service"
}

func (self handler) Handle(text string) (string, error) {
	if len(text) == 0 {
		return "", errors.New("name is not set")
	}
	err := self.client.Start(text)
	if err != nil {
		return "", err
	}
	go self.observe(text)
	return "sent", err
}

func (self *handler) observe(srv string) {
	cb := func(status string) {
		msg := fmt.Sprintf("serivce %s => %s", srv, status)
		self.sender.SendMsg(msg)
	}
	err := self.observer.Observe(srv, "inactive", "active", cb)
	if err == nil {
		msg := fmt.Sprintf("service %s started successfully", srv)
		self.sender.SendMsg(msg)
		return
	}
	msg := fmt.Sprintf("can not stop %s: %v", srv, err)
	self.sender.SendMsg(msg)
}
