package get_statuses

import (
	"ps2base/ps2proto"
	"ps2tg/proto"
	"strings"

	"gitlab.com/asvedr/giter"
)

type handler struct {
	client ps2proto.IClient
}

func New(client ps2proto.IClient) proto.ICommandHandler {
	return handler{client: client}
}

func (handler) Command() string {
	return "statuses"
}

func (handler) Help() string {
	return "show statuses of all the services"
}

func (self handler) Handle(text string) (string, error) {
	statuses, err := self.client.GetStatuses()
	if err != nil {
		return "", err
	}
	f := func(nm, stts string) string { return nm + ": " + stts }
	return strings.Join(giter.M2V(statuses, f), "\n"), nil
}
