package get_sys_info

import (
	"fmt"
	"ps2base/ps2proto"
	"ps2tg/proto"
)

type handler struct {
	client ps2proto.IClient
}

func New(client ps2proto.IClient) proto.ICommandHandler {
	return handler{client: client}
}

func (handler) Command() string {
	return "sys info"
}

func (handler) Help() string {
	return "get sys info"
}

func (self handler) Handle(text string) (string, error) {
	info, err := self.client.GetSysInfo()
	if err != nil {
		return "", err
	}
	msg := fmt.Sprintf(
		"temp: %s\nmem free %s\nmem total: %s\ncpu load: %s",
		info.Temp,
		info.MemFree,
		info.MemTotal,
		info.CpuLoad,
	)
	return msg, nil
}
