package main

import (
	"os"
	"ps2base/ps2consts"
	"ps2tg/app"

	"gitlab.com/asvedr/giter"
)

func main() {
	if len(os.Args) > 1 && os.Args[1] == "-h" {
		println(ps2consts.Version)
		return
	}
	giter.Gather(
		app.ProcAlert.Get().Run,
		app.ProcUi.Get().Run,
	)
}
