package utils

import (
	"strings"

	"gitlab.com/asvedr/giter"
)

func SplitToChunks(msg string, max_symbols int) []string {
	lines := strings.Split(msg, "\n")
	var result []string
	var current_msg string
	for {
		if len(lines) == 0 {
			break
		}
		line := lines[0]
		if len(current_msg)+len(line)+1 < max_symbols {
			current_msg += line + "\n"
			lines = lines[1:]
			continue
		}
		result = append(result, current_msg)
		if len(line)+1 < max_symbols {
			current_msg = line + "\n"
			lines = lines[1:]
		} else {
			current_msg = line[:max_symbols]
			lines[0] = line[max_symbols:]
		}
	}
	if len(current_msg) != 0 {
		result = append(result, current_msg)
	}

	return giter.FilterMap(
		result,
		func(s string) *string {
			s = strings.TrimSpace(s)
			if len(s) > 0 {
				return &s
			} else {
				return nil
			}
		},
	)
}
