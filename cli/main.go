package main

import (
	"fmt"
	"ps2base/ps2consts"
	"ps2cli/app"

	"gitlab.com/asvedr/cldi/cl"
)

func main() {
	descr := fmt.Sprintf(
		"CLI interface for pi2daemon daemon (version=%s)",
		ps2consts.Version,
	)
	cl.NewRunnerMulty(
		"",
		descr,
		cl.NewRunnerMulty(
			"config",
			"config operations",
			app.ValidateConfigEP.Get(),
			app.GenConfigEP.Get(),
		),
		cl.NewRunnerMulty(
			"get",
			"get info",
			app.GetSysInfoEP.Get(),
			app.GetAlertsEP.Get(),
			app.GetInfoEP.Get(),
			app.GetStatusesEP.Get(),
			app.GetAliveEP.Get(),
			app.GetLogsEP.Get(),
		),
		app.StartEP.Get(),
		app.RestartEP.Get(),
		app.StopEP.Get(),
		app.ForceAlertEP.Get(),
	).Run()
}
