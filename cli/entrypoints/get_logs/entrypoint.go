package get_logs

import (
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct{ client func() ps2proto.IClient }

type request struct {
	Srv string `short:"s" descr:"service name"`
	Max int    `short:"m" descr:"max lines" default:"100"`
}

func New(client func() ps2proto.IClient) cl.IEP {
	return ep{client: client}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "logs",
		Description: "get service logs",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self ep) Execute(_ string, args any) error {
	req := args.(request)
	logs, err := self.client().GetLogs(req.Srv, req.Max)
	if err != nil {
		return err
	}
	println(logs)
	return nil
}
