package start

import (
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct {
	client   func() ps2proto.IClient
	observer func() ps2proto.IClientStatusTransitionObserver
}

type request struct {
	Srv string `descr:"service name"`
}

func New(
	client func() ps2proto.IClient,
	observer func() ps2proto.IClientStatusTransitionObserver,
) cl.IEP {
	return ep{client: client, observer: observer}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "start",
		Description: "start service",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self ep) Execute(prog string, args any) error {
	req := args.(request)
	err := self.client().Start(req.Srv)
	if err != nil {
		return err
	}
	err = self.observer().Observe(
		req.Srv,
		"inactive",
		"active",
		func(status string) { println("status: " + status) },
	)
	if err == nil {
		println("Started!")
	}
	return err
}
