package gen_config

import (
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct {
	cp ps2proto.IConfigParser
	cd ps2proto.IConfigDumper
}

func New(
	cp ps2proto.IConfigParser,
	cd ps2proto.IConfigDumper,
) cl.IEP {
	return ep{cp: cp, cd: cd}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "gen",
		Description: "generate sample config",
	}
}

func (self ep) Execute(_ string, _ any) error {
	println(self.cd.Dump(self.cp.GenSample()))
	return nil
}
