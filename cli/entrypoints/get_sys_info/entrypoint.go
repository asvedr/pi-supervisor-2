package get_sys_info

import (
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct{ client func() ps2proto.IClient }

func New(client func() ps2proto.IClient) cl.IEP {
	return ep{client: client}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "sys-info",
		Description: "get sys info",
	}
}

func (self ep) Execute(_ string, _ any) error {
	info, err := self.client().GetSysInfo()
	if err != nil {
		return err
	}
	println("temp: " + info.Temp)
	println("mem free: " + info.MemFree)
	println("mem total: " + info.MemTotal)
	println("cpu load: " + info.CpuLoad)
	return nil
}
