package get_statuses

import (
	"fmt"
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct{ client func() ps2proto.IClient }

func New(client func() ps2proto.IClient) cl.IEP {
	return ep{client: client}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "statuses",
		Description: "get map {service: status}",
	}
}

func (self ep) Execute(prog string, request any) error {
	status_map, err := self.client().GetStatuses()
	if err != nil {
		return err
	}
	fmt.Printf("services: %d\n", len(status_map))
	for srv, status := range status_map {
		println(srv + ": " + status)
	}
	return nil
}
