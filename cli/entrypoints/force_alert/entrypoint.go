package force_alert

import (
	"ps2base/ps2proto"
	"ps2base/ps2types"

	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/genum"
	"gitlab.com/asvedr/giter"
)

type ep struct {
	client func() ps2proto.IClient
}

type request struct {
	Type string `short:"t" descr:"alert type" default:"notif"`
	Name string `descr:"alert name"`
	Msg  string `default:"" descr:"alert message"`
}

func (request) ChoiceType() []string {
	s_map := genum.MakeFromString[ps2types.AlertType]()
	return giter.MKeys(s_map)
}

func New(
	client func() ps2proto.IClient,
) cl.IEP {
	return ep{client: client}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "force-alert",
		Description: "force alert",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self ep) Execute(prog string, args any) error {
	req := args.(request)
	s_map := genum.MakeFromString[ps2types.AlertType]()
	alert := ps2types.Alert{
		Name: req.Name,
		Msg:  req.Msg,
		Type: s_map[req.Type],
	}
	err := self.client().ForceAlert(alert)
	if err != nil {
		return err
	}
	println("sent")
	return nil
}
