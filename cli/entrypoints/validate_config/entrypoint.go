package validate_config

import (
	"fmt"
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct {
	cp ps2proto.IConfigParser
	cd ps2proto.IConfigDumper
}

type request struct {
	Path *string `opt:"t"`
}

func New(
	cp ps2proto.IConfigParser,
	cd ps2proto.IConfigDumper,
) cl.IEP {
	return ep{cp: cp, cd: cd}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "validate",
		Description: "validate config",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self ep) Execute(_ string, args any) error {
	req := args.(request)
	path := self.cp.RevealPath()
	if req.Path != nil {
		path = *req.Path
	}
	conf, err := self.cp.Parse(path)
	if err != nil {
		return fmt.Errorf("path: %s, err: %v", path, err)
	}
	print(self.cd.Dump(conf))
	return nil
}
