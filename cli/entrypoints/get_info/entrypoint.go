package get_info

import (
	"fmt"
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct{ client func() ps2proto.IClient }

type request struct {
	Srv string `descr:"service name"`
}

func New(client func() ps2proto.IClient) cl.IEP {
	return ep{client: client}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "info",
		Description: "get service info",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self ep) Execute(_ string, args any) error {
	req := args.(request)
	info, err := self.client().GetInfo(req.Srv)
	if err != nil {
		return err
	}
	fmt.Printf(
		"service:%s\nmem_used:%s\nalive:%s\npid:%s\nlifetime:%s\n",
		info.Service,
		info.MemUsed,
		info.Alive,
		info.Pid,
		info.Lifetime,
	)
	return nil
}
