package restart

import (
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct{ client func() ps2proto.IClient }

type request struct {
	Srv string `descr:"service name"`
}

func New(client func() ps2proto.IClient) cl.IEP {
	return ep{client: client}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "restart",
		Description: "restart service",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self ep) Execute(_ string, args any) error {
	req := args.(request)
	err := self.client().Restart(req.Srv)
	if err == nil {
		println("signal sent")
	}
	return err
}
