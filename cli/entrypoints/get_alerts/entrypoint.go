package get_alerts

import (
	"fmt"
	"ps2base/ps2proto"
	"ps2base/ps2types"

	"gitlab.com/asvedr/cldi/cl"
)

var alert_type_str = map[ps2types.AlertType]string{
	ps2types.AlertTypeFail:  "[FAIL]",  // "🔴",
	ps2types.AlertTypeOk:    "[OK]",    // "🟢",
	ps2types.AlertTypeNotif: "[NOTIF]", // "🔵",
}

type ep struct{ client func() ps2proto.IClient }

type request struct {
	Max int `default:"10" descr:"max lines"`
}

func New(client func() ps2proto.IClient) cl.IEP {
	return ep{client: client}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "alerts",
		Description: "get alerts",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self ep) Execute(prog string, args any) error {
	req := args.(request)
	alerts, err := self.client().GetAlerts(req.Max)
	if err != nil {
		return err
	}
	fmt.Printf("alerts: %d\n", len(alerts))
	for _, alert := range alerts {
		fmt.Printf(
			"%s %s: %s\n",
			alert_type_str[alert.Type],
			alert.Name,
			alert.Msg,
		)
	}
	return nil
}
