package get_alive

import (
	"fmt"
	"ps2base/ps2proto"

	"gitlab.com/asvedr/cldi/cl"
)

type ep struct{ client func() ps2proto.IClient }

func New(client func() ps2proto.IClient) cl.IEP {
	return ep{client: client}
}

func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "alive",
		Description: "get alive services",
	}
}

func (self ep) Execute(_ string, _ any) error {
	services, err := self.client().GetAlive()
	if err != nil {
		return err
	}
	fmt.Printf("services: %d\n", len(services))
	for _, srv := range services {
		fmt.Printf(
			"%s:\n  mem: %s\n  pid: %d\n  lifetime: %s\n",
			srv.Service,
			srv.Mem,
			srv.Pid,
			srv.Lifetime,
		)
	}
	return nil
}
