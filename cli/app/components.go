package app

import (
	"ps2base/ps2client"
	"ps2base/ps2config_dumper"
	"ps2base/ps2config_parser"
	"ps2base/ps2proto"
	"ps2base/ps2status_transition_observer"
	"ps2base/ps2types"
	"time"

	"gitlab.com/asvedr/cldi/di"
)

var ConfigParser = di.Singleton[ps2proto.IConfigParser]{
	PureFunc: ps2config_parser.New,
}

var ConfigDumper = di.Singleton[ps2proto.IConfigDumper]{
	PureFunc: ps2config_dumper.New,
}

var Config = di.Singleton[*ps2types.Config]{
	ErrFunc: func() (*ps2types.Config, error) {
		p := ConfigParser.Get()
		return p.Parse(p.RevealPath())
	},
}

var Client = di.Singleton[ps2proto.IClient]{
	PureFunc: func() ps2proto.IClient {
		return ps2client.New(Config.Get())
	},
}

var StatusObserver = di.Singleton[ps2proto.IClientStatusTransitionObserver]{
	PureFunc: func() ps2proto.IClientStatusTransitionObserver {
		return ps2status_transition_observer.New(
			Client.Get(),
			time.Second*3,
			time.Millisecond*100,
		)
	},
}
