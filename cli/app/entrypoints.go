package app

import (
	"ps2cli/entrypoints/force_alert"
	"ps2cli/entrypoints/gen_config"
	"ps2cli/entrypoints/get_alerts"
	"ps2cli/entrypoints/get_alive"
	"ps2cli/entrypoints/get_info"
	"ps2cli/entrypoints/get_logs"
	"ps2cli/entrypoints/get_statuses"
	"ps2cli/entrypoints/get_sys_info"
	"ps2cli/entrypoints/restart"
	"ps2cli/entrypoints/start"
	"ps2cli/entrypoints/stop"
	"ps2cli/entrypoints/validate_config"

	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/cldi/di"
)

var GenConfigEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return gen_config.New(ConfigParser.Get(), ConfigDumper.Get())
	},
}

var ValidateConfigEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return validate_config.New(ConfigParser.Get(), ConfigDumper.Get())
	},
}

var GetAlertsEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return get_alerts.New(Client.Get)
	},
}

var GetInfoEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return get_info.New(Client.Get)
	},
}

var GetStatusesEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return get_statuses.New(Client.Get)
	},
}

var GetAliveEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return get_alive.New(Client.Get)
	},
}

var StartEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return start.New(Client.Get, StatusObserver.Get)
	},
}

var RestartEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return restart.New(Client.Get)
	},
}

var StopEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return stop.New(Client.Get, StatusObserver.Get)
	},
}

var GetLogsEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return get_logs.New(Client.Get)
	},
}

var ForceAlertEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return force_alert.New(Client.Get)
	},
}

var GetSysInfoEP = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return get_sys_info.New(Client.Get)
	},
}
