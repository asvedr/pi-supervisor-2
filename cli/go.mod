module ps2cli

go 1.21.3

require (
	gitlab.com/asvedr/cldi v0.4.0
	gitlab.com/asvedr/genum v1.0.0
	gitlab.com/asvedr/giter v1.3.0
)
