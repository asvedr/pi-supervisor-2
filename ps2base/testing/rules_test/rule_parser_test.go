package rules_test

import (
	"ps2base/ps2proto"
	"ps2base/ps2rule"
	"testing"
)

func parser() ps2proto.IRuleParser {
	return ps2rule.NewParser()
}

func TestParseGlobalOk(t *testing.T) {
	expr, err := parser().ParseGlob("free_mem < gb(2.1) and cpu > 1")
	if err != nil {
		t.Fatal(err)
	}
	if expr.Expr.ToString() != "and(<(free_mem,gb(2.1)),>(cpu,1))" {
		t.Fatal(expr.Expr.Raw())
	}
}

func TestParseGlobalBad(t *testing.T) {
	_, err := parser().ParseGlob("abc def")
	if err.Error() != "Unparsable sequence: def " {
		t.Fatal(err)
	}
}

func TestParseGlobalBadType(t *testing.T) {
	_, err := parser().ParseGlob("1 and cpu")
	if err.Error() != "func 'and': arg 1 has invalid type" {
		t.Fatal(err)
	}
}

func TestParseGlobalBadConst(t *testing.T) {
	_, err := parser().ParseGlob("proc_mem < gb(1)")
	if err.Error() != "Unknown value: proc_mem" {
		t.Fatal(err)
	}
}

func TestParseGlobalBadArg(t *testing.T) {
	_, err := parser().ParseGlob("free_mem < gb(1,2)")
	if err.Error() != "func 'gb' expected 1 args, found 2" {
		t.Fatal(err)
	}
}

func TestParseLocalOk(t *testing.T) {
	expr, err := parser().ParseProc("proc_mem < gb(1)")
	if err != nil {
		t.Fatal(err)
	}
	if expr.Expr.ToString() != "<(proc_mem,gb(1))" {
		t.Fatal(expr.Expr)
	}
}
