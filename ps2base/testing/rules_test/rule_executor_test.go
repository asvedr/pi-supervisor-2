package rules_test

import (
	"ps2base/ps2consts"
	"ps2base/ps2proto"
	"ps2base/ps2rule"
	"testing"
)

type test_case struct {
	expr string
	val  bool
}

func executor() ps2proto.IRuleExecutor {
	return ps2rule.NewExecutor()
}

var global_cases = []test_case{
	{expr: "temp > 46", val: true},
	{expr: "temp < 46", val: false},
	{expr: "free_mem = gb(1)", val: false},
	{expr: "cpu = 10", val: true},
	{expr: "cpu != 10", val: false},
	{expr: "!(cpu != 11)", val: false},
	{expr: "temp > 46 and free_mem > mb(2)", val: true},
	{expr: "temp <= 40 or free_mem > mb(2)", val: true},
	{expr: "temp > 40 or free_mem > mb(3)", val: true},
}

func TestExecGlobal(t *testing.T) {
	ex := executor()
	ex.GlobalCtx(
		46.5,
		int64((ps2consts.MemMB*2)+ps2consts.MemKB),
		10,
	)
	for _, tc := range global_cases {
		println(">> " + tc.expr)
		expr, err := parser().ParseGlob(tc.expr)
		if err != nil {
			t.Fatalf("%v|%v", tc.expr, err)
		}
		if ex.Execute(expr.Expr) != tc.val {
			t.Fatalf("%v != %v", ex.Execute(expr.Expr), tc.val)
		}
	}
}

var proc_cases = []test_case{
	{expr: "proc_mem = 100", val: true},
	{expr: "proc_mem < kb(1)", val: true},
	{expr: "proc_mem < 100", val: false},
}

func TestExecLocal(t *testing.T) {
	ex := executor()
	ex.LocalCtx(
		46.5,
		int64((ps2consts.MemMB*2)+ps2consts.MemKB),
		10,
		100,
	)
	for _, tc := range global_cases {
		println(">> " + tc.expr)
		expr, err := parser().ParseGlob(tc.expr)
		if err != nil {
			t.Fatalf("%v|%v", tc.expr, err)
		}
		if ex.Execute(expr.Expr) != tc.val {
			t.Fatalf("%v != %v", ex.Execute(expr.Expr), tc.val)
		}
	}
	for _, tc := range proc_cases {
		println(">> " + tc.expr)
		expr, err := parser().ParseProc(tc.expr)
		if err != nil {
			t.Fatalf("%v|%v", tc.expr, err)
		}
		if ex.Execute(expr.Expr) != tc.val {
			t.Fatalf("%v != %v", ex.Execute(expr.Expr), tc.val)
		}
	}
}
