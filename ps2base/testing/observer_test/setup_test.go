package observer_test

import (
	"ps2base/ps2proto"
	"ps2base/ps2status_transition_observer"
	"ps2base/ps2types"
	"sync"
	"time"
)

type client struct {
	mtx      sync.Mutex
	statuses map[string]string
}

func (self *client) SetStatus(srv, stat string) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.statuses[srv] = stat
}

func (self *client) DelStatus(srv string) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	delete(self.statuses, srv)
}

func (self *client) GetStatuses() (map[string]string, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	return self.statuses, nil
}

func (*client) GetAlerts(limit int) ([]ps2types.Alert, error) { panic("") }
func (*client) ForceAlert(ps2types.Alert) error               { panic("") }
func (*client) GetInfo(string) (ps2types.ApiInfo, error)      { panic("") }
func (*client) GetAlive() ([]ps2types.ApiService, error)      { panic("") }
func (*client) Start(string) error                            { panic("") }
func (*client) Restart(string) error                          { panic("") }
func (*client) Stop(string) error                             { panic("") }
func (*client) GetLogs(string, int) (string, error)           { panic("") }
func (*client) GetSysInfo() (ps2types.ApiSysInfo, error)      { panic("") }

type setup struct {
	updates  []string
	mtx      sync.Mutex
	client   *client
	interval time.Duration
	timeout  time.Duration
	observer ps2proto.IClientStatusTransitionObserver
}

func make_setup() setup {
	client := &client{
		statuses: map[string]string{},
	}
	interval := time.Millisecond * 20
	timeout := time.Millisecond * 100
	return setup{
		client:   client,
		interval: interval,
		timeout:  timeout,
		observer: ps2status_transition_observer.New(
			client,
			timeout,
			interval,
		),
	}
}

func (self *setup) log_status(stat string) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.updates = append(self.updates, stat)
}
