package observer_test

import (
	"ps2base/ps2errs"
	"reflect"
	"testing"
	"time"
)

func TestTransitionOk(t *testing.T) {
	setup := make_setup()
	setup.client.SetStatus("srv", "active")
	go func() {
		setup.client.SetStatus("srv", "active")
		time.Sleep(time.Millisecond * 30)
		setup.client.SetStatus("srv", "stopping")
		time.Sleep(time.Millisecond * 40)
		setup.client.SetStatus("srv", "inactive")
	}()
	err := setup.observer.Observe(
		"srv",
		"active",
		"inactive",
		setup.log_status,
	)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(setup.updates, []string{"stopping"}) {
		t.Fatal(setup.updates)
	}
}

func TestTransitionTimeout(t *testing.T) {
	setup := make_setup()
	setup.client.SetStatus("srv", "active")
	go func() {
		setup.client.SetStatus("srv", "active")
		time.Sleep(time.Millisecond * 30)
		setup.client.SetStatus("srv", "stopping")
	}()
	err := setup.observer.Observe(
		"srv",
		"active",
		"inactive",
		setup.log_status,
	)
	if !reflect.DeepEqual(setup.updates, []string{"stopping"}) {
		t.Fatal(setup.updates)
	}
	if !reflect.DeepEqual(
		err,
		ps2errs.ErrObserverTimeoutExceed{
			ExpectedStatus: "inactive",
			ActualStatus:   "stopping",
		},
	) {
		t.Fatal(err)
	}
}

func TestTransitionServiceNotInMap(t *testing.T) {
	setup := make_setup()
	setup.client.SetStatus("srv", "active")
	go func() {
		time.Sleep(time.Millisecond * 30)
		setup.client.DelStatus("srv")
	}()
	err := setup.observer.Observe(
		"srv",
		"active",
		"inactive",
		setup.log_status,
	)
	if len(setup.updates) != 0 {
		t.Fatal(setup.updates)
	}
	if err == nil || err.Error() != "service is not in the status map" {
		t.Fatal(err)
	}
}
