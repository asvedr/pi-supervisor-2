package config_parser_test

import (
	"ps2base/ps2config_parser"
	"ps2base/ps2proto"
	"ps2base/ps2rule"
	"ps2base/ps2types"
	"reflect"
	"testing"
	"time"
)

func parser() ps2proto.IConfigParser {
	return ps2config_parser.New()
}

func TestFullConf(t *testing.T) {
	rp := ps2rule.NewParser()
	got, err := parser().Parse("./full_config.yml")
	if err != nil {
		t.Fatal(err)
	}
	proc_rule, _ := rp.ParseProc("temp > 50")
	hot_rule, _ := rp.ParseGlob("temp > 60")
	busy_rule, _ := rp.ParseGlob("cpu > 50")
	ev_started := ps2types.EventStarted
	var admin_id int64 = 1234
	expected := ps2types.Config{
		Services: []ps2types.ConfService{
			{
				Name:   "srv1",
				Prog:   "/bin/sh",
				Args:   []string{"echo", "hi"},
				Start:  false,
				KillOn: []ps2types.Rule{proc_rule},
				Env:    map[string]string{"A": "1", "B": "2"},
			},
			{
				Name:  "srv2",
				Prog:  "/bin/cmd",
				Start: true,
			},
		},
		Api: ps2types.ConfApi{Port: 1234},
		Daemon: ps2types.ConfDaemon{
			ProcLogLen:      100,
			RestartTimeout:  time.Second,
			SysCacheTimeout: time.Second * 150,
			AlerterTimeout:  time.Second * 3,
			KillerTimeout:   time.Second * 4,
			BusTimeout:      time.Second * 5,
			SysTempSource:   "/bin/temp",
			BusMaxLen:       99,
		},
		Alerts: []ps2types.ConfAlert{
			{
				Name:             "hot",
				RepeatTimeout:    time.Second * 2,
				ThresholdTimeout: time.Second * 3,
				Rule:             &hot_rule,
			},
			{
				Name:             "busy",
				RepeatTimeout:    time.Second * 30,
				ThresholdTimeout: time.Second * 5,
				Rule:             &busy_rule,
			},
			{
				Name:             "on start",
				RepeatTimeout:    time.Second * 30,
				ThresholdTimeout: time.Second * 5,
				Event:            &ev_started,
			},
		},
		Tg: ps2types.ConfTg{
			AdminId:            &admin_id,
			Token:              "abc123",
			CheckAlertsTimeout: time.Second * 10,
			MaxMsgLen:          3,
			MaxTry:             1,
		},
	}
	if !reflect.DeepEqual(got.Services, expected.Services) {
		t.Fatal(got.Services)
	}
	if !reflect.DeepEqual(got.Daemon, expected.Daemon) {
		t.Fatal(got.Daemon)
	}
	if !reflect.DeepEqual(got.Api, expected.Api) {
		t.Fatal(got.Api)
	}
	if !reflect.DeepEqual(got.Alerts, expected.Alerts) {
		t.Fatal(got.Alerts)
	}
	if !reflect.DeepEqual(got.Tg, expected.Tg) {
		t.Fatal(got.Tg)
	}
}

func TestMinimal(t *testing.T) {
	got, err := parser().Parse("./minimal_config.yml")
	if err != nil {
		t.Fatal(err)
	}
	expected := ps2types.Config{
		Services: []ps2types.ConfService{
			{
				Name:  "srv",
				Prog:  "/bin/srv",
				Start: true,
			},
		},
		Api: ps2types.ConfApi{Port: 8790},
		Daemon: ps2types.ConfDaemon{
			ProcLogLen:      1000,
			RestartTimeout:  time.Second,
			SysCacheTimeout: time.Second,
			AlerterTimeout:  time.Millisecond * 100,
			KillerTimeout:   time.Millisecond * 100,
			BusTimeout:      time.Millisecond * 50,
			SysTempSource:   "/sys/class/thermal/thermal_zone0/temp",
			BusMaxLen:       1000,
		},
		Tg: ps2types.ConfTg{
			AdminId:            nil,
			Token:              "",
			CheckAlertsTimeout: time.Second,
			MaxMsgLen:          100,
			MaxTry:             10,
		},
	}
	if !reflect.DeepEqual(got.Services, expected.Services) {
		t.Fatal(got.Services)
	}
	if !reflect.DeepEqual(got.Daemon, expected.Daemon) {
		t.Fatal(got.Daemon)
	}
	if !reflect.DeepEqual(got.Api, expected.Api) {
		t.Fatal(got.Api)
	}
	if !reflect.DeepEqual(got.Alerts, expected.Alerts) {
		t.Fatal(got.Alerts)
	}
	if !reflect.DeepEqual(got.Tg, expected.Tg) {
		t.Fatal(got.Tg)
	}
}

func TestGenSample(t *testing.T) {
	// no panic
	parser().GenSample()
}
