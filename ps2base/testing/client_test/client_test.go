package client_test

import (
	"encoding/json"
	"fmt"
	"ps2base/ps2client"
	"ps2base/ps2config_parser"
	"ps2base/ps2types"
	"reflect"
	"strings"
	"testing"
)

type http_client struct {
	url  string
	req  string
	resp string
}

func (self http_client) Request(url string, body any, response any) error {
	if self.url != url {
		return fmt.Errorf("expected url: %s, found: %s", self.url, url)
	}
	req_bts, err := json.Marshal(body)
	if err != nil {
		return err
	}
	req := string(req_bts)
	if self.req != req {
		return fmt.Errorf("expected req: %s, found: %s", self.req, req)
	}
	if response == nil {
		return nil
	}
	str_resp, casted := response.(*string)
	if casted {
		*str_resp = self.resp
		return nil
	}
	return json.Unmarshal([]byte(self.resp), response)
}

func TestGetAlerts(t *testing.T) {
	client := ps2client.NewWithHttp(
		ps2config_parser.New().GenSample(),
		http_client{
			url:  "http://localhost:8790/get-alerts/",
			req:  `{"limit":10}`,
			resp: `{"alerts": [{"name": "A", "msg": "!"}]}`,
		},
	)
	alerts, err := client.GetAlerts(10)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(
		alerts,
		[]ps2types.Alert{{Name: "A", Msg: "!"}},
	) {
		t.Fatal(alerts)
	}
}

func TestGetInfo(t *testing.T) {
	lines := []string{
		`"service":"srv"`,
		`"mem_used":"2mb"`,
		`"alive":"true"`,
		`"pid":"3"`,
		`"lifetime":"10s"`,
	}
	resp := "{" + strings.Join(lines, ",") + "}"
	client := ps2client.NewWithHttp(
		ps2config_parser.New().GenSample(),
		http_client{
			url:  "http://localhost:8790/get-info/",
			req:  `{"service":"srv"}`,
			resp: resp,
		},
	)
	info, err := client.GetInfo("srv")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(
		info,
		ps2types.ApiInfo{
			Service:  "srv",
			MemUsed:  "2mb",
			Alive:    "true",
			Pid:      "3",
			Lifetime: "10s",
		},
	) {
		t.Fatal(info)
	}
}

func TestGetStatuses(t *testing.T) {
	client := ps2client.NewWithHttp(
		ps2config_parser.New().GenSample(),
		http_client{
			url:  "http://localhost:8790/get-statuses/",
			req:  `null`,
			resp: `{"a": "active", "b":"inactive"}`,
		},
	)
	statuses, err := client.GetStatuses()
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(
		statuses,
		map[string]string{"a": "active", "b": "inactive"},
	) {
		t.Fatal(statuses)
	}
}

func TestGetAlive(t *testing.T) {
	client := ps2client.NewWithHttp(
		ps2config_parser.New().GenSample(),
		http_client{
			url:  "http://localhost:8790/get-alive/",
			req:  `null`,
			resp: `{"services": [{"service":"a","mem":"2kb","pid":1,"lifetime":"1s"}]}`,
		},
	)
	services, err := client.GetAlive()
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(
		services,
		[]ps2types.ApiService{
			{
				Service:  "a",
				Mem:      "2kb",
				Pid:      1,
				Lifetime: "1s",
			},
		},
	) {
		t.Fatal(services)
	}
}

func TestStart(t *testing.T) {
	client := ps2client.NewWithHttp(
		ps2config_parser.New().GenSample(),
		http_client{
			url:  "http://localhost:8790/start/",
			req:  `{"service":"a"}`,
			resp: `ok`,
		},
	)
	err := client.Start("a")
	if err != nil {
		t.Fatal(err)
	}
}

func TestRestart(t *testing.T) {
	client := ps2client.NewWithHttp(
		ps2config_parser.New().GenSample(),
		http_client{
			url:  "http://localhost:8790/restart/",
			req:  `{"service":"a"}`,
			resp: `ok`,
		},
	)
	err := client.Restart("a")
	if err != nil {
		t.Fatal(err)
	}
}

func TestStop(t *testing.T) {
	client := ps2client.NewWithHttp(
		ps2config_parser.New().GenSample(),
		http_client{
			url:  "http://localhost:8790/stop/",
			req:  `{"service":"a"}`,
			resp: `ok`,
		},
	)
	err := client.Stop("a")
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetLogs(t *testing.T) {
	client := ps2client.NewWithHttp(
		ps2config_parser.New().GenSample(),
		http_client{
			url:  "http://localhost:8790/get-logs/",
			req:  `{"service":"a","max_rows":5}`,
			resp: "line1\nline2\nline3",
		},
	)
	logs, err := client.GetLogs("a", 5)
	if err != nil {
		t.Fatal(err)
	}
	if logs != "line1\nline2\nline3" {
		t.Fatal(logs)
	}
}
