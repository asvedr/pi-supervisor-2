package ps2consts

var Version = "N/A"

const EnvVarConfPath = "PI_SUPERVISOR_CONFIG_PATH"
const DefaultConfPath = "/etc/pi-supervisor-2.yml"

const MemKB uint64 = 1000
const MemMB uint64 = MemKB * 1000
const MemGB uint64 = MemMB * 1000

const RuleConstTemp = "temp"
const RuleConstFreeMem = "free_mem"
const RuleConstCpu = "cpu"
const RuleConstProcMem = "proc_mem"

const DefaultPort uint16 = 8790
const DefaultLogLen = 1000
const DefaultTempSource = "/sys/class/thermal/thermal_zone0/temp"
const DefaultLongTimeout_str = "1s"
const DefaultShortTimeoutStr = "0.1s"
const DefaultBusTimeoutStr = "0.05s"
const DefaultRepeatTimeout = "30s"
const DefaultThresholdTimeout = "5s"
const DefaultBusMaxLen = 1000
const DefaultTgCheckAlertsTimeout = "1s"
const DefaultTgMaxMsgLen = 100
const DefaultTgMaxTry = 10

const GetAlertsUrl = "/get-alerts/"
const GetInfoUrl = "/get-info/"
const GetStatusesUrl = "/get-statuses/"
const GetAliveUrl = "/get-alive/"
const StartUrl = "/start/"
const RestartUrl = "/restart/"
const StopUrl = "/stop/"
const GetLogsUrl = "/get-logs/"
const GetSysInfoUrl = "/get-sys-info/"
const ForceAlertUrl = "/force-alert/"
