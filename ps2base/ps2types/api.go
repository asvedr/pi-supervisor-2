package ps2types

type ApiGetAlertsRequest struct {
	Limit int `json:"limit"`
}

type ApiGetAlertsResponse struct {
	Alerts []Alert `json:"alerts"`
}

type ApiGetLogsRequest struct {
	Service string `json:"service"`
	MaxRows int    `json:"max_rows"`
}

type ApiInfo struct {
	Service  string `json:"service"`
	MemUsed  string `json:"mem_used"`
	Alive    string `json:"alive"`
	Pid      string `json:"pid"`
	Lifetime string `json:"lifetime"`
}

type ApiGetAliveResponse struct {
	Services []ApiService `json:"services"`
}

type ApiService struct {
	Service  string `json:"service"`
	Mem      string `json:"mem"`
	Pid      int64  `json:"pid"`
	Lifetime string `json:"lifetime"`
}

type ApiServiceAction struct {
	Service string `json:"service"`
}

type ApiSysInfo struct {
	Temp     string `json:"temp"`
	MemFree  string `json:"mem_free"`
	MemTotal string `json:"mem_total"`
	CpuLoad  string `json:"cpu"`
}
