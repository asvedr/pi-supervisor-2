package ps2types

import (
	"fmt"

	"gitlab.com/asvedr/gexpr"
)

type Alert struct {
	Name string    `json:"name"`
	Msg  string    `json:"msg"`
	Type AlertType `json:"type"`
}

type Rule struct {
	Raw    string
	Consts map[string]struct{}
	Expr   gexpr.INode
}

type Event int

const (
	EventStarted Event = iota
	EventProcKilled
)

type AlertType int

const (
	AlertTypeFail  AlertType = 0
	AlertTypeOk    AlertType = 1
	AlertTypeNotif AlertType = 2
)

func (self Event) GetVariants() []any {
	return []any{EventStarted, EventProcKilled}
}

func (self Event) String() string {
	switch self {
	case EventStarted:
		return "started"
	case EventProcKilled:
		return "killed"
	default:
		panic(fmt.Sprintf("unexpected: %d", self))
	}
}

func (self AlertType) GetVariants() []any {
	return []any{AlertTypeFail, AlertTypeOk, AlertTypeNotif}
}

func (self AlertType) String() string {
	switch self {
	case AlertTypeFail:
		return "fail"
	case AlertTypeOk:
		return "ok"
	case AlertTypeNotif:
		return "notif"
	default:
		panic(fmt.Sprintf("unexpected: %d", self))
	}
}
