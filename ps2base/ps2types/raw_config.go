package ps2types

type RawConfig struct {
	Services []RawConfService `yaml:"services"`
	Api      RawConfApi       `yaml:"api"`
	Daemon   RawConfDaemon    `yaml:"daemon"`
	Alerts   []RawConfAlert   `yaml:"alerts"`
	Tg       RawConfTg        `yaml:"tg"`
}

type RawConfApi struct {
	Port *uint16 `yaml:"port"`
}

type RawConfDaemon struct {
	ProcLogLen      *int    `yaml:"proc_log_len"`
	RestartTimeout  *string `yaml:"restart_timeout"`
	SysCacheTimeout *string `yaml:"sys_cache_timeout"`
	AlerterTimeout  *string `yaml:"alerter_timeout"`
	KillerTimeout   *string `yaml:"killer_timeout"`
	BusTimeout      *string `yaml:"bus_timeout"`
	SysTempSource   *string `yaml:"sys_temp_source"`
	BusMaxLen       *int    `yaml:"bus_max_len"`
}

type RawConfService struct {
	Name   string            `yaml:"name"`
	Prog   string            `yaml:"command"`
	Args   []string          `yaml:"args"`
	Start  *bool             `yaml:"autostart"`
	KillOn []string          `yaml:"kill_on"`
	Env    map[string]string `yaml:"env"`
}

type RawConfAlert struct {
	Name             string  `yaml:"name"`
	RepeatTimeout    *string `yaml:"repeat_timeout"`
	ThresholdTimeout *string `yaml:"threshold_timeout"`
	Rule             string  `yaml:"rule"`
	Event            string  `yaml:"event"`
}

type RawConfTg struct {
	AdminId            *int64  `yaml:"admin_id"`
	Token              string  `yaml:"token"`
	CheckAlertsTimeout *string `yaml:"check_alerts_timeout"`
	MaxMsgLen          *int    `yaml:"max_msg_len"`
	MaxTry             *int    `yaml:"max_try"`
}
