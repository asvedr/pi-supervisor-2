package ps2types

import (
	"time"

	_ "gopkg.in/yaml.v3"
)

type Config struct {
	Services []ConfService
	Api      ConfApi
	Daemon   ConfDaemon
	Alerts   []ConfAlert
	Tg       ConfTg
}

type ConfApi struct {
	Port uint16
}

type ConfDaemon struct {
	ProcLogLen      int
	RestartTimeout  time.Duration
	SysCacheTimeout time.Duration
	AlerterTimeout  time.Duration
	KillerTimeout   time.Duration
	BusTimeout      time.Duration
	BusMaxLen       int
	SysTempSource   string
}

type ConfService struct {
	Name   string
	Prog   string
	Args   []string
	Start  bool
	KillOn []Rule
	Env    map[string]string
}

type ConfAlert struct {
	Name             string
	RepeatTimeout    time.Duration
	ThresholdTimeout time.Duration
	Rule             *Rule
	Event            *Event
}

type ConfTg struct {
	AdminId            *int64
	Token              string
	CheckAlertsTimeout time.Duration
	MaxMsgLen          int
	MaxTry             int
}
