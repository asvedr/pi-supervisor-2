package ps2rule

import (
	"errors"
	"fmt"

	"gitlab.com/asvedr/gexpr"
)

type validator struct {
	funs   map[string]fun_spec
	consts map[string]vtype
}

func new_rule_validator() *validator {
	return &validator{
		funs:   rule_funcs(),
		consts: rule_consts(),
	}
}

func (self *validator) Validate(node gexpr.INode) error {
	tp, err := self.reveal_type(node)
	if err == nil && tp != type_bool {
		err = errors.New("rule type must be bool")
	}
	return err
}

func (self *validator) reveal_type(node gexpr.INode) (vtype, error) {
	switch node.Type() {
	case gexpr.NodeTypeInt:
		return type_num, nil
	case gexpr.NodeTypeReal:
		return type_num, nil
	case gexpr.NodeTypeBool:
		return type_bool, nil
	case gexpr.NodeTypeVar:
		return self.reveal_type_var(node)
	case gexpr.NodeTypeCall:
		return self.reveal_type_call(node)
	default:
		return 0, errors.New("invalid expression: " + node.ToString())
	}
}

func (self *validator) reveal_type_var(node gexpr.INode) (vtype, error) {
	val, found := self.consts[node.VarName()]
	if !found {
		return 0, errors.New("invalid var: " + node.VarName())
	}
	return val, nil
}

func (self *validator) reveal_type_call(node gexpr.INode) (vtype, error) {
	spec, found := self.funs[node.Func()]
	if !found {
		return 0, errors.New("unknown function: " + node.Func())
	}
	if len(node.Args()) != len(spec.args) {
		return 0, fmt.Errorf(
			"func '%s' expected %d args, found %d",
			node.Func(),
			len(spec.args),
			len(node.Args()),
		)
	}
	for i, arg := range node.Args() {
		tp, err := self.reveal_type(arg)
		if err != nil {
			return 0, err
		}
		if spec.args[i] != tp {
			return 0, fmt.Errorf(
				"func '%s': arg %d has invalid type",
				node.Func(),
				i+1,
			)
		}
	}
	return spec.res, nil
}
