package ps2rule

import (
	"ps2base/ps2consts"
	"ps2base/ps2proto"
	t "ps2base/ps2types"

	"gitlab.com/asvedr/gexpr"
)

type parser struct {
	glob_parser gexpr.IParser
	proc_parser gexpr.IParser
	validator   *validator
}

func NewParser() ps2proto.IRuleParser {
	bin_ops := map[string]int{
		">": 1, "<": 1, "<=": 1, ">=": 1, "=": 1, "!=": 1,
		"and": 0, "or": 0,
	}
	g := gexpr.NewParser(gexpr.ParserConfig{
		BinOperators: bin_ops,
		UnOperators:  []string{"!"},
		Funcs:        []string{"kb", "mb", "gb"},
		Consts: []string{
			ps2consts.RuleConstTemp,
			ps2consts.RuleConstFreeMem,
			ps2consts.RuleConstCpu,
		},
	})
	p := gexpr.NewParser(gexpr.ParserConfig{
		BinOperators: bin_ops,
		UnOperators:  []string{"!"},
		Funcs:        []string{"kb", "mb", "gb"},
		Consts: []string{
			ps2consts.RuleConstTemp,
			ps2consts.RuleConstFreeMem,
			ps2consts.RuleConstCpu,
			ps2consts.RuleConstProcMem,
		},
	})
	return &parser{
		glob_parser: g,
		proc_parser: p,
		validator:   new_rule_validator(),
	}
}

func (self *parser) ParseGlob(src string) (t.Rule, error) {
	node, err := self.glob_parser.Parse(src)
	if err != nil {
		return t.Rule{}, err
	}
	err = self.validator.Validate(node)
	return t.Rule{
		Expr:   node,
		Raw:    src,
		Consts: extract_consts(node),
	}, err
}

func (self *parser) ParseProc(src string) (t.Rule, error) {
	node, err := self.proc_parser.Parse(src)
	if err != nil {
		return t.Rule{}, err
	}
	err = self.validator.Validate(node)
	return t.Rule{
		Expr:   node,
		Raw:    src,
		Consts: extract_consts(node),
	}, err
}

func extract_consts(node gexpr.INode) map[string]struct{} {
	set := map[string]struct{}{}
	var rec_down func(node gexpr.INode)
	rec_down = func(node gexpr.INode) {
		switch node.Type() {
		case gexpr.NodeTypeVar:
			set[node.VarName()] = struct{}{}
		case gexpr.NodeTypeCall:
			for _, child := range node.Args() {
				rec_down(child)
			}
		case gexpr.NodeTypeMap:
			panic("not implemented")
		case gexpr.NodeTypeArr:
			panic("not implemented")
		}
	}
	rec_down(node)
	return set
}
