package ps2rule

import (
	"ps2base/ps2consts"

	"gitlab.com/asvedr/giter"
)

type vtype int

const (
	type_num vtype = iota
	type_bool
)

type fun_spec struct {
	args []vtype
	res  vtype
	exec func([]any) any
}

func rule_consts() map[string]vtype {
	return map[string]vtype{
		ps2consts.RuleConstTemp:    type_num,
		ps2consts.RuleConstFreeMem: type_num,
		ps2consts.RuleConstCpu:     type_num,
		ps2consts.RuleConstProcMem: type_num,
	}
}

func rule_funcs() map[string]fun_spec {
	flip := func(f func([]any) any) func([]any) any {
		return func(args []any) any {
			return f(giter.V(args[1], args[0]))
		}
	}
	not := func(f func([]any) any) func([]any) any {
		return func(args []any) any {
			val := f(args)
			return !(val.(bool))
		}
	}

	return map[string]fun_spec{
		">": {
			args: giter.V(type_num, type_num),
			res:  type_bool,
			exec: f_gt,
		},
		"<": {
			args: giter.V(type_num, type_num),
			res:  type_bool,
			exec: flip(f_gt),
		},
		">=": {
			args: giter.V(type_num, type_num),
			res:  type_bool,
			exec: not(flip(f_gt)),
		},
		"<=": {
			args: giter.V(type_num, type_num),
			res:  type_bool,
			exec: not(f_gt),
		},
		"=": {
			args: giter.V(type_num, type_num),
			res:  type_bool,
			exec: f_eq,
		},
		"!=": {
			args: giter.V(type_num, type_num),
			res:  type_bool,
			exec: not(f_eq),
		},
		"and": {
			args: giter.V(type_bool, type_bool),
			res:  type_bool,
			exec: func(args []any) any {
				return args[0].(bool) && args[1].(bool)
			},
		},
		"or": {
			args: giter.V(type_bool, type_bool),
			res:  type_bool,
			exec: func(args []any) any {
				return args[0].(bool) || args[1].(bool)
			},
		},
		"!": {
			args: giter.V(type_bool),
			res:  type_bool,
			exec: func(val []any) any { return !val[0].(bool) },
		},
		"kb": {
			args: giter.V(type_num),
			res:  type_num,
			exec: m_cast_size(ps2consts.MemKB),
		},
		"mb": {
			args: giter.V(type_num),
			res:  type_num,
			exec: m_cast_size(ps2consts.MemMB),
		},
		"gb": {
			args: giter.V(type_num),
			res:  type_num,
			exec: m_cast_size(ps2consts.MemGB),
		},
	}
}

func force_float(val any) float64 {
	fval, is_float := val.(float64)
	if is_float {
		return fval
	}
	return float64(val.(int64))
}

func force_int(val any) int64 {
	ival, is_int := val.(int64)
	if is_int {
		return ival
	}
	return int64(val.(float64))
}

func f_gt(args []any) any {
	_, is_float_a := args[0].(float64)
	_, is_float_b := args[1].(float64)
	if is_float_a || is_float_b {
		return force_float(args[0]) > force_float(args[1])
	} else {
		return args[0].(int64) > args[1].(int64)
	}
}

func f_eq(args []any) any {
	return force_int(args[0]) == force_int(args[1])
}

func m_cast_size(mult uint64) func([]any) any {
	return func(args []any) any {
		ival, is_int := args[0].(int64)
		if is_int {
			return int64(mult) * ival
		}
		return int64(args[0].(float64) * float64(mult))
	}
}
