package ps2rule

import (
	"ps2base/ps2consts"
	"ps2base/ps2proto"

	"gitlab.com/asvedr/gexpr"
	"gitlab.com/asvedr/giter"
)

type executor struct {
	funs map[string]fun_spec
	ctx  map[string]any
}

func NewExecutor() ps2proto.IRuleExecutor {
	return &executor{
		funs: rule_funcs(),
		ctx:  map[string]any{},
	}
}

func (self *executor) GlobalCtx(
	temp float64,
	free_mem int64,
	cpu float64,
) {
	self.ctx[ps2consts.RuleConstTemp] = temp
	self.ctx[ps2consts.RuleConstFreeMem] = free_mem
	self.ctx[ps2consts.RuleConstCpu] = cpu
	delete(self.ctx, ps2consts.RuleConstProcMem)
}

func (self *executor) LocalCtx(
	temp float64,
	free_mem int64,
	cpu float64,
	proc_mem int64,
) {
	self.ctx[ps2consts.RuleConstTemp] = temp
	self.ctx[ps2consts.RuleConstFreeMem] = free_mem
	self.ctx[ps2consts.RuleConstCpu] = cpu
	self.ctx[ps2consts.RuleConstProcMem] = proc_mem
}

func (self *executor) Execute(node gexpr.INode) bool {
	val := self.exec_node(node)
	return val.(bool)
}

func (self *executor) exec_node(node gexpr.INode) any {
	switch node.Type() {
	case gexpr.NodeTypeInt:
		return node.Int()
	case gexpr.NodeTypeReal:
		return node.Real()
	case gexpr.NodeTypeBool:
		return node.Bool()
	case gexpr.NodeTypeVar:
		return self.ctx[node.VarName()]
	case gexpr.NodeTypeCall:
		return self.exec_call(node)
	default:
		panic("unexpected node: " + node.ToString())
	}
}

func (self *executor) exec_call(node gexpr.INode) any {
	fun := self.funs[node.Func()]
	args := giter.Map(node.Args(), self.exec_node)
	return fun.exec(args)
}
