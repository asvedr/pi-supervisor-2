module ps2base

go 1.21.3

require (
	gitlab.com/asvedr/gexpr v0.2.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	gitlab.com/asvedr/genum v1.0.0 // indirect
	gitlab.com/asvedr/giter v1.3.0 // indirect
)
