package ps2client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"ps2base/ps2errs"
)

type http_client struct{}

func (http_client) Request(url string, body any, response any) error {
	wrap_err := func(err error) error {
		return &ps2errs.ErrCanNotMakeRequest{
			Url:   url,
			Cause: err,
		}
	}
	bts, err := json.Marshal(body)
	if err != nil {
		return wrap_err(err)
	}
	buf := bytes.NewBuffer(bts)
	resp, err := http.Post(url, "application/json", buf)
	if err != nil {
		return wrap_err(err)
	}
	if resp.StatusCode != 200 {
		bts, _ = io.ReadAll(resp.Body)
		log.Printf("RESPONSE: %s", string(bts))
		return wrap_err(fmt.Errorf("StatusCode=%d", resp.StatusCode))
	}
	bts, err = io.ReadAll(resp.Body)
	if err != nil {
		return wrap_err(err)
	}
	if response == nil {
		return nil
	}
	str_resp, casted := response.(*string)
	if casted {
		*str_resp = string(bts)
		return nil
	}
	err = json.Unmarshal(bts, response)
	if err != nil {
		err = wrap_err(err)
	}
	return err
}
