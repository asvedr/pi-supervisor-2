package ps2client

import (
	"fmt"
	"ps2base/ps2consts"
	"ps2base/ps2proto"
	"ps2base/ps2types"
)

type client struct {
	base_url    string
	http_client ps2proto.IHttpClient
}

func NewWithHttp(config *ps2types.Config, http_client ps2proto.IHttpClient) ps2proto.IClient {
	base_url := fmt.Sprintf("http://localhost:%d", config.Api.Port)
	return client{
		http_client: http_client,
		base_url:    base_url,
	}
}

func New(config *ps2types.Config) ps2proto.IClient {
	return NewWithHttp(config, http_client{})
}

func (self client) GetAlerts(limit int) ([]ps2types.Alert, error) {
	var resp ps2types.ApiGetAlertsResponse
	err := self.http_client.Request(
		self.url(ps2consts.GetAlertsUrl),
		ps2types.ApiGetAlertsRequest{Limit: limit},
		&resp,
	)
	return resp.Alerts, err
}

func (self client) ForceAlert(alert ps2types.Alert) error {
	return self.http_client.Request(
		self.url(ps2consts.ForceAlertUrl),
		alert,
		nil,
	)
}

func (self client) GetInfo(srv string) (ps2types.ApiInfo, error) {
	var resp ps2types.ApiInfo
	err := self.http_client.Request(
		self.url(ps2consts.GetInfoUrl),
		ps2types.ApiServiceAction{Service: srv},
		&resp,
	)
	return resp, err
}

func (self client) GetStatuses() (map[string]string, error) {
	resp := map[string]string{}
	err := self.http_client.Request(
		self.url(ps2consts.GetStatusesUrl),
		nil,
		&resp,
	)
	return resp, err
}

func (self client) GetAlive() ([]ps2types.ApiService, error) {
	var resp ps2types.ApiGetAliveResponse
	err := self.http_client.Request(
		self.url(ps2consts.GetAliveUrl),
		nil,
		&resp,
	)
	return resp.Services, err
}

func (self client) Start(srv string) error {
	return self.http_client.Request(
		self.url(ps2consts.StartUrl),
		ps2types.ApiServiceAction{Service: srv},
		nil,
	)
}

func (self client) Restart(srv string) error {
	return self.http_client.Request(
		self.url(ps2consts.RestartUrl),
		ps2types.ApiServiceAction{Service: srv},
		nil,
	)
}

func (self client) Stop(srv string) error {
	return self.http_client.Request(
		self.url(ps2consts.StopUrl),
		ps2types.ApiServiceAction{Service: srv},
		nil,
	)
}

func (self client) GetLogs(srv string, max_rows int) (string, error) {
	result := ""
	err := self.http_client.Request(
		self.url(ps2consts.GetLogsUrl),
		ps2types.ApiGetLogsRequest{Service: srv, MaxRows: max_rows},
		&result,
	)
	return result, err
}

func (self client) GetSysInfo() (ps2types.ApiSysInfo, error) {
	var result ps2types.ApiSysInfo
	err := self.http_client.Request(
		self.url(ps2consts.GetSysInfoUrl),
		nil,
		&result,
	)
	return result, err
}

func (self client) url(path string) string {
	return self.base_url + path
}
