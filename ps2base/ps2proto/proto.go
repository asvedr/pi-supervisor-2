package ps2proto

import (
	"ps2base/ps2types"

	"gitlab.com/asvedr/gexpr"
)

type IConfigParser interface {
	RevealPath() string
	Parse(path string) (*ps2types.Config, error)
	GenSample() *ps2types.Config
}

type IConfigDumper interface {
	Dump(*ps2types.Config) string
}

type IRuleParser interface {
	ParseGlob(string) (ps2types.Rule, error)
	ParseProc(string) (ps2types.Rule, error)
}

type IRuleExecutor interface {
	GlobalCtx(temp float64, free_mem int64, cpu float64)
	LocalCtx(temp float64, free_mem int64, cpu float64, proc_mem int64)
	Execute(node gexpr.INode) bool
}

type IClient interface {
	GetAlerts(limit int) ([]ps2types.Alert, error)
	ForceAlert(ps2types.Alert) error
	GetInfo(string) (ps2types.ApiInfo, error)
	GetStatuses() (map[string]string, error)
	GetAlive() ([]ps2types.ApiService, error)
	Start(string) error
	Restart(string) error
	Stop(string) error
	GetLogs(string, int) (string, error)
	GetSysInfo() (ps2types.ApiSysInfo, error)
}

type IClientStatusTransitionObserver interface {
	Observe(
		srv string,
		current_status string,
		terminal_status string,
		callback func(string),
	) error
}

type IHttpClient interface {
	Request(url string, body any, response any) error
}
