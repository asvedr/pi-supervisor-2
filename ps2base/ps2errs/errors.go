package ps2errs

import "fmt"

type ErrCanNotReadConfig struct {
	Cause error
}

type ErrCanNotParseConfig struct {
	Section string
	Cause   error
}

type ErrCanNotMakeRequest struct {
	Url   string
	Cause error
}

type ErrObserverTimeoutExceed struct {
	ActualStatus   string
	ExpectedStatus string
}

func (e ErrCanNotReadConfig) Error() string {
	return "ErrCanNotReadConfig: " + e.Cause.Error()
}

func (e ErrCanNotParseConfig) Error() string {
	if len(e.Section) == 0 {
		return "ErrCanNotParseConfig: " + e.Cause.Error()
	}
	return fmt.Sprintf(
		"ErrCanNotParseConfig(%s): %v",
		e.Section,
		e.Cause.Error(),
	)
}

func (e ErrCanNotMakeRequest) Error() string {
	return fmt.Sprintf(
		"ErrCanNotMakeRequest(url=%s): %v",
		e.Url,
		e.Cause,
	)
}

func (e ErrObserverTimeoutExceed) Error() string {
	return fmt.Sprintf(
		"ErrObserverTimeoutExceed(expected=%s, actual=%s)",
		e.ExpectedStatus,
		e.ActualStatus,
	)
}
