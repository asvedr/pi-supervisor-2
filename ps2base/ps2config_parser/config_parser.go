package ps2config_parser

import (
	"errors"
	"os"
	"ps2base/ps2consts"
	c "ps2base/ps2consts"
	e "ps2base/ps2errs"
	p "ps2base/ps2proto"
	r "ps2base/ps2rule"
	"ps2base/ps2types"
	t "ps2base/ps2types"
	"strconv"
	"strings"
	"time"

	"gitlab.com/asvedr/genum"
	"gitlab.com/asvedr/giter"
	"gopkg.in/yaml.v3"
)

type config_parser struct {
	rule_parser   p.IRuleParser
	str_event_map map[string]t.Event
}

func New() p.IConfigParser {
	return &config_parser{
		rule_parser:   r.NewParser(),
		str_event_map: genum.MakeFromString[t.Event](),
	}
}

func (self *config_parser) RevealPath() string {
	for _, line := range os.Environ() {
		pair := strings.SplitN(line, "=", 2)
		if strings.ToUpper(pair[0]) == c.EnvVarConfPath {
			return pair[1]
		}
	}
	return c.DefaultConfPath
}

func (self *config_parser) GenSample() *t.Config {
	services := []t.RawConfService{{Name: "srv", Prog: "/bin/srv"}}
	alerts := []t.RawConfAlert{
		{Name: "ev started", Event: t.EventStarted.String()},
		{Name: "ev temp", Rule: "temp > 45"},
	}
	raw := t.RawConfig{Services: services, Alerts: alerts}
	val, err := self.raw_to_config(raw)
	if err != nil {
		panic(err.Error())
	}
	return val
}

func (self *config_parser) Parse(path string) (*t.Config, error) {
	bts, err := os.ReadFile(path)
	if err != nil {
		return nil, e.ErrCanNotReadConfig{Cause: err}
	}
	var raw t.RawConfig
	err = yaml.Unmarshal(bts, &raw)
	if err != nil {
		return nil, e.ErrCanNotParseConfig{Cause: err}
	}
	return self.raw_to_config(raw)
}

func (self *config_parser) raw_to_config(raw t.RawConfig) (*t.Config, error) {
	services, err := giter.MapErr(raw.Services, self.parse_conf_service)
	if err != nil {
		return nil, e.ErrCanNotParseConfig{
			Section: "services",
			Cause:   err,
		}
	}
	if len(services) == 0 {
		return nil, e.ErrCanNotParseConfig{
			Cause: errors.New("service list is empty"),
		}
	}
	daemon, err := self.parse_conf_daemon(raw.Daemon)
	if err != nil {
		return nil, e.ErrCanNotParseConfig{
			Section: "daemon",
			Cause:   err,
		}
	}
	alerts, err := giter.MapErr(raw.Alerts, self.parse_alert)
	if err != nil {
		return nil, err
	}
	tg, err := self.parse_conf_tg(raw.Tg)
	if err != nil {
		return nil, err
	}
	config := t.Config{
		Services: services,
		Api:      parse_api(raw.Api),
		Daemon:   daemon,
		Alerts:   alerts,
		Tg:       tg,
	}
	return &config, nil
}

func parse_api(raw t.RawConfApi) t.ConfApi {
	return t.ConfApi{Port: or_default(raw.Port, c.DefaultPort)}
}

func (self *config_parser) parse_conf_service(raw t.RawConfService) (t.ConfService, error) {
	if len(raw.Name) == 0 {
		return t.ConfService{}, e.ErrCanNotParseConfig{
			Cause: errors.New("service name not set"),
		}
	}
	if len(raw.Prog) == 0 {
		return t.ConfService{}, e.ErrCanNotParseConfig{
			Section: "service: " + raw.Name,
			Cause:   errors.New("prog not set"),
		}
	}
	kill_on, err := giter.MapErr(raw.KillOn, self.rule_parser.ParseProc)
	return t.ConfService{
		Name:   raw.Name,
		Prog:   raw.Prog,
		Args:   raw.Args,
		Start:  or_default(raw.Start, true),
		KillOn: kill_on,
		Env:    raw.Env,
	}, err
}

func (self *config_parser) parse_alert(raw t.RawConfAlert) (t.ConfAlert, error) {
	if len(raw.Name) == 0 {
		return t.ConfAlert{}, e.ErrCanNotParseConfig{
			Cause: errors.New("alert name not set"),
		}
	}
	if (len(raw.Rule) == 0) == (len(raw.Event) == 0) {
		return t.ConfAlert{}, e.ErrCanNotParseConfig{
			Section: "alert: " + raw.Name,
			Cause:   errors.New("alert rule OR event must be set"),
		}
	}
	rule, event, err := self.parse_rule_or_event(raw)
	if err != nil {
		return t.ConfAlert{}, err
	}
	repeat_timeout, err := self.parse_time(
		or_default(raw.RepeatTimeout, c.DefaultRepeatTimeout),
	)
	if err != nil {
		return t.ConfAlert{}, e.ErrCanNotParseConfig{
			Section: "alerts.repeat_timeout",
			Cause:   err,
		}
	}
	threshold_timeout, err := self.parse_time(
		or_default(raw.ThresholdTimeout, c.DefaultThresholdTimeout),
	)
	if err != nil {
		return t.ConfAlert{}, e.ErrCanNotParseConfig{
			Section: "alerts.threshold_timeout",
			Cause:   err,
		}
	}
	return t.ConfAlert{
		Name:             raw.Name,
		RepeatTimeout:    repeat_timeout,
		ThresholdTimeout: threshold_timeout,
		Rule:             rule,
		Event:            event,
	}, nil
}

func (self *config_parser) parse_rule_or_event(raw t.RawConfAlert) (*t.Rule, *t.Event, error) {
	if len(raw.Rule) != 0 {
		rule, err := self.rule_parser.ParseGlob(raw.Rule)
		return &rule, nil, err
	} else {
		event, err := self.parse_event(raw.Event)
		return nil, &event, err
	}
}

func (self *config_parser) parse_event(src string) (t.Event, error) {
	val, found := self.str_event_map[src]
	if !found {
		return 0, e.ErrCanNotParseConfig{
			Section: "alerts.event",
			Cause:   errors.New("invalid event: " + src),
		}
	}
	return val, nil
}

func (self *config_parser) parse_conf_daemon(raw t.RawConfDaemon) (t.ConfDaemon, error) {
	werr := func(s string, c error) (t.ConfDaemon, error) {
		return t.ConfDaemon{}, e.ErrCanNotParseConfig{Section: s, Cause: c}
	}
	restart_timeout, err := self.parse_time(
		or_default(raw.RestartTimeout, c.DefaultLongTimeout_str),
	)
	if err != nil {
		return werr("daemon.restart_timeout", err)
	}
	cache_timeout, err := self.parse_time(
		or_default(raw.SysCacheTimeout, c.DefaultLongTimeout_str),
	)
	if err != nil {
		return werr("daemon.sys_cache_timeout", err)
	}
	alerter_timeout, err := self.parse_time(
		or_default(raw.AlerterTimeout, c.DefaultShortTimeoutStr),
	)
	if err != nil {
		return werr("daemon.alerter_timeout", err)
	}
	killer_timeout, err := self.parse_time(
		or_default(raw.KillerTimeout, c.DefaultShortTimeoutStr),
	)
	if err != nil {
		return werr("daemon.killer_timeout", err)
	}
	bus_timeout, err := self.parse_time(
		or_default(raw.BusTimeout, c.DefaultBusTimeoutStr),
	)
	if err != nil {
		return werr("daemon.bus_timeout", err)
	}
	return t.ConfDaemon{
		ProcLogLen:      or_default(raw.ProcLogLen, c.DefaultLogLen),
		RestartTimeout:  restart_timeout,
		SysCacheTimeout: cache_timeout,
		AlerterTimeout:  alerter_timeout,
		KillerTimeout:   killer_timeout,
		BusTimeout:      bus_timeout,
		SysTempSource: or_default(
			raw.SysTempSource,
			c.DefaultTempSource,
		),
		BusMaxLen: or_default(raw.BusMaxLen, c.DefaultBusMaxLen),
	}, nil
}

func (self *config_parser) parse_conf_tg(raw ps2types.RawConfTg) (ps2types.ConfTg, error) {
	timeout_raw := or_default(
		raw.CheckAlertsTimeout,
		ps2consts.DefaultTgCheckAlertsTimeout,
	)
	timeout, err := self.parse_time(timeout_raw)
	if err != nil {
		return ps2types.ConfTg{}, e.ErrCanNotParseConfig{
			Section: "tg.check_alerts_timeout",
			Cause:   err,
		}
	}
	return ps2types.ConfTg{
		AdminId:            raw.AdminId,
		Token:              raw.Token,
		CheckAlertsTimeout: timeout,
		MaxMsgLen:          or_default(raw.MaxMsgLen, ps2consts.DefaultTgMaxMsgLen),
		MaxTry:             or_default(raw.MaxTry, ps2consts.DefaultTgMaxTry),
	}, nil
}

func (self *config_parser) parse_time(src string) (time.Duration, error) {
	as_runes := []rune(src)
	if len(as_runes) == 0 {
		return 0, errors.New("empty value")
	}
	with := func(mult time.Duration) (time.Duration, error) {
		str_num := string(as_runes[:len(as_runes)-1])
		num, err := strconv.ParseFloat(str_num, 64)
		if err != nil {
			return 0, err
		}
		return time.Duration(num * float64(mult)), nil
	}
	switch as_runes[len(as_runes)-1] {
	case 's':
		return with(time.Second)
	case 'm':
		return with(time.Minute)
	case 'h':
		return with(time.Hour)
	default:
		return 0, errors.New("expected suffix s, m or h")
	}
}

func or_default[T any](src *T, dflt T) T {
	if src != nil {
		return *src
	}
	return dflt
}
