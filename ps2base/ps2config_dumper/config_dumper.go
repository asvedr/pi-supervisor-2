package ps2config_dumper

import (
	"fmt"
	p "ps2base/ps2proto"
	t "ps2base/ps2types"
	"time"

	"gitlab.com/asvedr/giter"
	"gopkg.in/yaml.v3"
)

type config_dumper struct{}

func New() p.IConfigDumper {
	return config_dumper{}
}

func (self config_dumper) Dump(config *t.Config) string {
	raw := t.RawConfig{
		Services: giter.Map(config.Services, self.dump_service),
		Api:      self.dump_api(config.Api),
		Daemon:   self.dump_daemon(config.Daemon),
		Alerts:   giter.Map(config.Alerts, self.dump_alert),
		Tg:       self.dump_tg(config.Tg),
	}
	data, err := yaml.Marshal(raw)
	if err != nil {
		panic(err.Error())
	}
	return string(data)
}

func (self config_dumper) dump_api(config t.ConfApi) t.RawConfApi {
	return t.RawConfApi{Port: &config.Port}
}

func (self config_dumper) dump_daemon(config t.ConfDaemon) t.RawConfDaemon {
	return t.RawConfDaemon{
		ProcLogLen:      &config.ProcLogLen,
		RestartTimeout:  self.dump_timeout(config.RestartTimeout),
		SysCacheTimeout: self.dump_timeout(config.SysCacheTimeout),
		AlerterTimeout:  self.dump_timeout(config.AlerterTimeout),
		KillerTimeout:   self.dump_timeout(config.KillerTimeout),
		BusTimeout:      self.dump_timeout(config.BusTimeout),
		SysTempSource:   &config.SysTempSource,
		BusMaxLen:       &config.BusMaxLen,
	}
}

func (self config_dumper) dump_service(config t.ConfService) t.RawConfService {
	return t.RawConfService{
		Name:   config.Name,
		Prog:   config.Prog,
		Args:   config.Args,
		Start:  &config.Start,
		KillOn: giter.Map(config.KillOn, self.dump_rule),
		Env:    config.Env,
	}
}

func (self config_dumper) dump_alert(config t.ConfAlert) t.RawConfAlert {
	var rule, event string
	if config.Event != nil {
		event = config.Event.String()
	} else {
		rule = self.dump_rule(*config.Rule)
	}
	return t.RawConfAlert{
		Name:             config.Name,
		RepeatTimeout:    self.dump_timeout(config.RepeatTimeout),
		ThresholdTimeout: self.dump_timeout(config.ThresholdTimeout),
		Rule:             rule,
		Event:            event,
	}
}

func (self config_dumper) dump_tg(config t.ConfTg) t.RawConfTg {
	return t.RawConfTg{
		AdminId:            config.AdminId,
		Token:              config.Token,
		CheckAlertsTimeout: self.dump_timeout(config.CheckAlertsTimeout),
		MaxMsgLen:          &config.MaxMsgLen,
		MaxTry:             &config.MaxTry,
	}
}

func (config_dumper) dump_rule(rule t.Rule) string {
	return rule.Raw
}

func (config_dumper) dump_timeout(tm time.Duration) *string {
	str := fmt.Sprintf("%ds", int64(tm.Seconds()))
	return &str
}
