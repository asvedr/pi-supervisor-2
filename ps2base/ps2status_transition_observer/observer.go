package ps2status_transition_observer

import (
	"errors"
	"ps2base/ps2errs"
	"ps2base/ps2proto"
	"time"
)

type observer struct {
	client   ps2proto.IClient
	timeout  time.Duration
	interval time.Duration
}

func New(
	client ps2proto.IClient,
	timeout time.Duration,
	interval time.Duration,
) ps2proto.IClientStatusTransitionObserver {
	return &observer{
		client:   client,
		timeout:  timeout,
		interval: interval,
	}
}

func (self *observer) Observe(
	srv string,
	current_status string,
	terminal_status string,
	callback func(string),
) error {
	begin := time.Now()
	for time.Now().Sub(begin) < self.timeout {
		status, err := self.get_status(srv)
		if err != nil {
			return err
		}
		if status == terminal_status {
			return nil
		}
		if status != current_status {
			current_status = status
			callback(status)
		}
		time.Sleep(self.interval)
	}
	status, err := self.get_status(srv)
	if err != nil {
		return err
	}
	if status != terminal_status {
		return ps2errs.ErrObserverTimeoutExceed{
			ExpectedStatus: terminal_status,
			ActualStatus:   status,
		}
	}
	return nil
}

func (self *observer) get_status(service string) (string, error) {
	statuses, err := self.client.GetStatuses()
	if err != nil {
		return "", err
	}
	status, found := statuses[service]
	if !found {
		return "", errors.New("service is not in the status map")
	}
	return status, nil
}
