#!/bin/sh
set -e
echo "\n\n====PS2BASE====\n\n"
sh -c "cd ps2base && go test ./..."
echo "\n\n====DAEMON====\n\n"
sh -c "cd daemon && go test ./..."
echo "\n\n====CLI====\n\n"
sh -c "cd cli && go test ./..."
echo "\n\n====TG====\n\n"
sh -c "cd tg && go test ./..."

