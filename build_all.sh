#!/bin/sh
export VERSION="$(cat version.txt)"
export DAEMON_EXE="ps2daemon"
export CLI_EXE="ps2cli"
export TG_EXE="ps2tg"
export LD_FLAGS="-ldflags='-X ps2base/ps2consts.Version=$VERSION'"
set -e
echo "Version: $VERSION"

echo "Build daemon"
CMD="cd daemon && go build $LD_FLAGS"
echo "$CMD"
sh -c "$CMD"
mv daemon/ps2daemon $DAEMON_EXE
echo "Saved as $DAEMON_EXE"

echo "Build cli"
CMD="cd cli && go build $LD_FLAGS"
echo "$CMD"
sh -c "$CMD"
mv cli/ps2cli $CLI_EXE
echo "Saved as $CLI_EXE"

echo "Build tg"
CMD="cd tg && go build $LD_FLAGS"
echo "$CMD"
sh -c "$CMD"
mv tg/ps2tg $TG_EXE
echo "Saved as $TG_EXE"

