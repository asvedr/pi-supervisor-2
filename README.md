# PI Supervisor

## What is it?
application for running services on raspberry-pi.

Features:
- start/stop/autorestart services
- check sys info(temperature/mem/cpu)
- kill services on conditions
- control via telegram or cli
- alerts
- configure in yaml

## How to install
1. Choose tag
2. Run job `build_linux_arm`
3. Download artifacts
4. Add `ps2daemon` to autostart in your system
5. Add `ps2tg` in system autostart OR include it into yaml config

## How to use
Send commands via telegram bot(ps2tg exe) OR via ps2cli(console tool). Collect alers automaticly via telegram bot OR via `ps2cli alerts`

## Configuration
Config must be placed at `/etc/pi-supervisor-2.yml`
OR set path into variable `PI_SUPERVISOR_CONFIG_PATH`

Actual config can be validated via `ps2cli validate-config` OR generated via `ps2cli gen-config`

### Config format
```yaml
# At least one service must be declared
services:
    # service name
  - name: srv1 
    # path to executable
    command: "/bin/sh"
    # [optional] cli args
    args: ["echo", "hi"]
    # true/false
    autostart: no
    # [optional] reasons to kill. See rule syntax
    kill_on:
      - temp > 50
    # [optional] env vars
    env:
      A: 1
      B: 2
api:
  # [optional] server port(default=8790)
  port: 1234
daemon:
  # [optional] max collected log len for child service (default=1000)
  proc_log_len: 100
  # [optional] restart timeout for child service (default=1s)
  restart_timeout: 1s
  # [optional] timeout between sys info update (default=1s)
  sys_cache_timeout: 2.5m
  # [optional] timeout between alerter iterations (default=0.1s)
  alerter_timeout: 3s
  # [optional] timeout between killer iterations (default=0.1s)
  killer_timeout: 4s
  # [optional] bus reader timeout (default=0.05s)
  bus_timeout: 5s
  # [optional] system temperature source (default=/sys/class/thermal/thermal_zone0/temp)
  sys_temp_source: /bin/temp
  # [optional] max events in bus (default=1000)
  bus_max_len: 99
alerts:
    # name
  - name: hot
    # [optional] (default=30s)
    repeat_timeout: 2s
    # [optional] (default=5s)
    threshold_timeout: 3s
    # This is alert via rule. See rule syntax
    rule: temp > 60
  - name: on start
    # this is alert via events. See events
    event: started
# tg section is required if you want to use telegram bot
tg:
  # tg user id
  admin_id: 1234
  # robot token
  token: "abc123"
  # [optional] (default=1s)
  check_alerts_timeout: 10s
  # [optional] max symbols in one message (default=100)
  max_msg_len: 3
  # [optional] max retries to send message (default=10)
  max_try: 1
```

### Events
- "started": daemon started
- "killed": service killed

### Rule syntax
Expression has regular syntax. Toplevel must have boolean type

kill rule:
```
binary operators: >, <, <=, >=, =, !=, and, or
unary operators: !
constants:
  temp - temperature
  free_mem - free memory
  cpu - cpu load
funcs:
  kb(n) - n * 1000
  mb(n) - n * 1000 * 1000
  gb(n) - n * 1000 * 1000 * 1000

rule example: temp > 50.5 and free_mem < mb(2)
```

alert rule:
```
binary operators: >, <, <=, >=, =, !=, and, or
unary operators: !
constants:
  temp - temperature
  free_mem - free memory
  cpu - cpu load
  proc_mem - memory used by process
funcs:
  kb(n) - n * 1000
  mb(n) - n * 1000 * 1000
  gb(n) - n * 1000 * 1000 * 1000

rule example: !(cpu > 2) or proc_mem > mb(10)
```