package utils

import (
	"ps2base/ps2types"
	"time"
)

type Bus[T any] struct {
	inner   chan T
	timeout time.Duration
}

func NewBus[T any](config *ps2types.Config) Bus[T] {
	return Bus[T]{
		inner:   make(chan T, config.Daemon.BusMaxLen),
		timeout: config.Daemon.BusTimeout,
	}
}

func (self Bus[T]) Put(val T) {
	self.inner <- val
}

func (self Bus[T]) ReadAll() []T {
	result := []T{}
	for {
		select {
		case val, ok := <-self.inner:
			if !ok {
				panic("channel is closed")
			}
			result = append(result, val)
		case <-time.After(self.timeout):
			return result
		}
	}
}

func (self Bus[T]) ReadLimit(limit int) []T {
	result := []T{}
	for i := 0; i < limit; i += 1 {
		select {
		case val, ok := <-self.inner:
			if !ok {
				panic("channel is closed")
			}
			result = append(result, val)
		case <-time.After(self.timeout):
			return result
		}
	}
	return result
}
