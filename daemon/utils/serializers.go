package utils

import (
	"fmt"
	"ps2daemon/constants"
	"strings"
)

func SerMem(val uint64) string {
	with := func(mult uint64, suf string) string {
		i_part := val / mult
		f_part := float64(val-(i_part*mult)) / float64(mult)
		f_full := float64(i_part) + f_part
		return ser_float(f_full) + suf
	}

	if val >= constants.MemGB {
		return with(constants.MemGB, "gb")
	}
	if val >= constants.MemMB {
		return with(constants.MemMB, "mb")
	}
	if val >= constants.MemKB {
		return with(constants.MemKB, "kb")
	}
	return with(1, "b")
}

func ser_float(val float64) string {
	split := strings.SplitN(fmt.Sprintf("%.3f", val), ".", 2)
	i_part := split[0]
	f_part := split[1]
	for len(f_part) > 0 {
		trimmed := strings.TrimSuffix(f_part, "0")
		if trimmed == f_part {
			break
		}
		f_part = trimmed
	}
	return i_part + "." + f_part
}
