package proc_alerter_test

import (
	"errors"
	"ps2base/ps2config_parser"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"reflect"
	"testing"
	"time"
)

func TestEventAlert(t *testing.T) {
	config := ps2config_parser.New().GenSample()
	config.Alerts[1].ThresholdTimeout = time.Second
	config.Daemon.BusTimeout = time.Millisecond
	setup := make_setup(config)
	setup.info_collector.sys_info = entities.SysInfo{
		Temp:    entities.Result[float64]{Val: 50},
		MemFree: entities.Result[uint64]{Val: 1000},
		CpuLoad: entities.Result[float64]{Val: 10},
	}

	setup.alerter.DoStep()
	alerts := setup.alert_bus.ReadAll()
	exp := []ps2types.Alert{{Name: "ev started", Type: ps2types.AlertTypeNotif}}
	if !reflect.DeepEqual(alerts, exp) {
		t.Fatal(alerts)
	}

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) > 0 {
		t.Fatal(alerts)
	}
}

func TestNewAlert(t *testing.T) {
	config := ps2config_parser.New().GenSample()
	config.Alerts[1].ThresholdTimeout = time.Duration(0)
	config.Alerts = config.Alerts[1:]
	config.Daemon.BusTimeout = time.Millisecond
	setup := make_setup(config)
	setup.info_collector.sys_info = entities.SysInfo{
		Temp:    entities.Result[float64]{Val: 50},
		MemFree: entities.Result[uint64]{Val: 1000},
		CpuLoad: entities.Result[float64]{Val: 10},
	}

	setup.alerter.DoStep()
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	exp := []ps2types.Alert{{Name: "ev temp", Msg: "temp > 45"}}
	if !reflect.DeepEqual(alerts, exp) {
		t.Fatal(alerts)
	}
}

func TestRepeatAlert(t *testing.T) {
	repeat_timeout := time.Millisecond * 100

	config := ps2config_parser.New().GenSample()
	config.Alerts[1].ThresholdTimeout = time.Duration(0)
	config.Alerts[1].RepeatTimeout = repeat_timeout
	config.Alerts = config.Alerts[1:]
	config.Daemon.BusTimeout = time.Millisecond
	setup := make_setup(config)
	setup.info_collector.sys_info = entities.SysInfo{
		Temp:    entities.Result[float64]{Val: 50},
		MemFree: entities.Result[uint64]{Val: 1000},
		CpuLoad: entities.Result[float64]{Val: 10},
	}

	setup.alerter.DoStep()
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	exp := []ps2types.Alert{{Name: "ev temp", Msg: "temp > 45"}}
	if !reflect.DeepEqual(alerts, exp) {
		t.Fatal(alerts)
	}

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	time.Sleep(repeat_timeout)

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if !reflect.DeepEqual(alerts, exp) {
		t.Fatal(alerts)
	}
}

func TestBlinkAlert(t *testing.T) {
	config := ps2config_parser.New().GenSample()
	config.Alerts[1].ThresholdTimeout = time.Duration(0)
	config.Alerts = config.Alerts[1:]
	config.Daemon.BusTimeout = time.Millisecond
	setup := make_setup(config)
	setup.info_collector.sys_info = entities.SysInfo{
		Temp:    entities.Result[float64]{Val: 50},
		MemFree: entities.Result[uint64]{Val: 1000},
		CpuLoad: entities.Result[float64]{Val: 10},
	}

	setup.alerter.DoStep()
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	setup.info_collector.sys_info.Temp.Val = 30

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	setup.info_collector.sys_info.Temp.Val = 50

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}
}

func TestInfoErrAlert(t *testing.T) {
	config := ps2config_parser.New().GenSample()
	config.Alerts[1].ThresholdTimeout = time.Duration(0)
	config.Alerts = config.Alerts[1:]
	config.Daemon.BusTimeout = time.Millisecond
	setup := make_setup(config)
	setup.info_collector.sys_info = entities.SysInfo{
		Temp:    entities.Result[float64]{Val: 50},
		MemFree: entities.Result[uint64]{Val: 1000},
		CpuLoad: entities.Result[float64]{Val: 10},
	}

	setup.alerter.DoStep()
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	setup.info_collector.sys_info.Temp.Err = errors.New("foo")

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	setup.info_collector.sys_info.Temp.Err = nil

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	exp := []ps2types.Alert{{Name: "ev temp", Msg: "temp > 45"}}
	if !reflect.DeepEqual(alerts, exp) {
		t.Fatal(alerts)
	}
}

func TestOkAlert(t *testing.T) {
	config := ps2config_parser.New().GenSample()
	config.Alerts[1].ThresholdTimeout = time.Millisecond * 100
	config.Alerts = config.Alerts[1:]
	config.Daemon.BusTimeout = time.Millisecond
	setup := make_setup(config)
	setup.info_collector.sys_info = entities.SysInfo{
		Temp:    entities.Result[float64]{Val: 50},
		MemFree: entities.Result[uint64]{Val: 1000},
		CpuLoad: entities.Result[float64]{Val: 10},
	}

	setup.alerter.DoStep()
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	time.Sleep(time.Millisecond * 100)

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	exp := []ps2types.Alert{{
		Name: "ev temp", Msg: "temp > 45", Type: ps2types.AlertTypeFail,
	}}
	if !reflect.DeepEqual(alerts, exp) {
		t.Fatal(alerts)
	}

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	setup.info_collector.sys_info.Temp.Val = 20

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}

	time.Sleep(time.Millisecond * 110)

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	exp = []ps2types.Alert{{
		Name: "ev temp",
		Msg:  "temp > 45",
		Type: ps2types.AlertTypeOk,
	}}
	if !reflect.DeepEqual(alerts, exp) {
		t.Fatal(alerts)
	}

	setup.alerter.DoStep()
	alerts = setup.alert_bus.ReadAll()
	if len(alerts) != 0 {
		t.Fatal(alerts)
	}
}
