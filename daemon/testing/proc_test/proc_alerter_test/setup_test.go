package proc_alerter_test

import (
	"ps2base/ps2rule"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/processes/alerter"
	"ps2daemon/proto"
	"ps2daemon/utils"
)

type info_collector struct {
	sys_info entities.SysInfo
}

type setup struct {
	config         *ps2types.Config
	info_collector *info_collector
	alert_bus      utils.Bus[ps2types.Alert]
	events_bus     utils.Bus[entities.EventCtx]
	alerter        proto.IStepProcess
}

func (self *info_collector) GetSysInfo() entities.SysInfo {
	return self.sys_info
}
func (self *info_collector) GetProcInfo(int64) entities.ProcExtInfo { panic("") }

func make_setup(config *ps2types.Config) setup {
	info_collector := &info_collector{}
	alert_bus := utils.NewBus[ps2types.Alert](config)
	events_bus := utils.NewBus[entities.EventCtx](config)
	alerter := alerter.New(
		config,
		info_collector,
		ps2rule.NewExecutor(),
		alert_bus,
		events_bus,
	)
	return setup{
		config:         config,
		info_collector: info_collector,
		alert_bus:      alert_bus,
		events_bus:     events_bus,
		alerter:        alerter,
	}
}
