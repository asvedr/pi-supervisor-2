package proc_killer_test

import (
	"errors"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"reflect"
	"testing"
)

func TestNoKill(t *testing.T) {
	setup := make_setup()
	setup.info_collector.sys_info = entities.SysInfo{
		Temp: entities.Result[float64]{Val: 10},
	}
	setup.start_stop.alive = []entities.ProcFullInfo{
		{
			ProcExtInfo: entities.ProcExtInfo{
				MemUsed: entities.Result[uint64]{Val: 5},
				Alive:   entities.Result[bool]{Val: true},
			},
			Service: "srv",
		},
	}
	setup.start_stop.statuses = map[string]entities.ProcessStatus{
		"srv": entities.ProcessStatusActive,
	}

	setup.killer.DoStep()

	if len(setup.start_stop.stop_calls) != 0 {
		t.Fatal(setup.start_stop.stop_calls)
	}
	events := setup.events_bus.ReadAll()
	if len(events) > 0 {
		t.Fatal(events)
	}
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) > 0 {
		t.Fatal(alerts)
	}
}

func TestKillByTemp(t *testing.T) {
	setup := make_setup()
	setup.info_collector.sys_info = entities.SysInfo{
		Temp: entities.Result[float64]{Val: 40},
	}
	setup.start_stop.alive = []entities.ProcFullInfo{
		{
			ProcExtInfo: entities.ProcExtInfo{
				MemUsed: entities.Result[uint64]{Val: 5},
				Alive:   entities.Result[bool]{Val: true},
			},
			Service: "srv",
		},
	}
	setup.start_stop.statuses = map[string]entities.ProcessStatus{
		"srv": entities.ProcessStatusActive,
	}

	setup.killer.DoStep()

	exp_call := []string{"srv"}
	if !reflect.DeepEqual(setup.start_stop.stop_calls, exp_call) {
		t.Fatal(setup.start_stop.stop_calls)
	}
	events := setup.events_bus.ReadAll()
	exp_evs := []entities.EventCtx{
		{
			Event: ps2types.EventProcKilled,
			Ctx:   "srv: temp > 30",
		},
	}
	if !reflect.DeepEqual(events, exp_evs) {
		t.Fatal(events)
	}
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) > 0 {
		t.Fatal(alerts)
	}
}

func TestKillByMem(t *testing.T) {
	setup := make_setup()
	setup.info_collector.sys_info = entities.SysInfo{
		Temp: entities.Result[float64]{Val: 20},
	}
	setup.start_stop.alive = []entities.ProcFullInfo{
		{
			ProcExtInfo: entities.ProcExtInfo{
				MemUsed: entities.Result[uint64]{Val: 50},
				Alive:   entities.Result[bool]{Val: true},
			},
			Service: "srv",
		},
	}
	setup.start_stop.statuses = map[string]entities.ProcessStatus{
		"srv": entities.ProcessStatusActive,
	}

	setup.killer.DoStep()

	exp_call := []string{"srv"}
	if !reflect.DeepEqual(setup.start_stop.stop_calls, exp_call) {
		t.Fatal(setup.start_stop.stop_calls)
	}
	events := setup.events_bus.ReadAll()
	exp_evs := []entities.EventCtx{
		{
			Event: ps2types.EventProcKilled,
			Ctx:   "srv: proc_mem > 10",
		},
	}
	if !reflect.DeepEqual(events, exp_evs) {
		t.Fatal(events)
	}
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) > 0 {
		t.Fatal(alerts)
	}
}

func TestKillErr(t *testing.T) {
	setup := make_setup()
	setup.info_collector.sys_info = entities.SysInfo{
		Temp: entities.Result[float64]{Val: 50},
	}
	setup.start_stop.alive = []entities.ProcFullInfo{
		{Service: "srv"},
	}
	setup.start_stop.statuses = map[string]entities.ProcessStatus{
		"srv": entities.ProcessStatusActive,
	}
	setup.start_stop.stop_resps["srv"] = errors.New("oops")

	setup.killer.DoStep()

	exp_call := []string{"srv"}
	if !reflect.DeepEqual(setup.start_stop.stop_calls, exp_call) {
		t.Fatal(setup.start_stop.stop_calls)
	}
	events := setup.events_bus.ReadAll()
	if len(events) > 0 {
		t.Fatal(events)
	}
	alerts := setup.alert_bus.ReadAll()
	exp_alerts := []ps2types.Alert{
		{Name: "can not stop service srv", Msg: "oops"},
	}
	if !reflect.DeepEqual(alerts, exp_alerts) {
		t.Fatal(alerts)
	}
}

func TestDoNotKillStopping(t *testing.T) {
	setup := make_setup()
	setup.info_collector.sys_info = entities.SysInfo{
		Temp: entities.Result[float64]{Val: 20},
	}
	setup.start_stop.alive = []entities.ProcFullInfo{
		{
			ProcExtInfo: entities.ProcExtInfo{
				MemUsed: entities.Result[uint64]{Val: 50},
				Alive:   entities.Result[bool]{Val: true},
			},
			Service: "srv",
		},
	}
	setup.start_stop.statuses = map[string]entities.ProcessStatus{
		"srv": entities.ProcessStatusStopping,
	}

	setup.killer.DoStep()

	if len(setup.start_stop.stop_calls) != 0 {
		t.Fatal(setup.start_stop.stop_calls)
	}
	events := setup.events_bus.ReadAll()
	if len(events) != 0 {
		t.Fatal(events)
	}
	alerts := setup.alert_bus.ReadAll()
	if len(alerts) > 0 {
		t.Fatal(alerts)
	}
}
