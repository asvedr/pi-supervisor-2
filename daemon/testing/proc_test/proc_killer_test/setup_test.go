package proc_killer_test

import (
	"ps2base/ps2config_parser"
	"ps2base/ps2rule"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/processes/killer"
	"ps2daemon/proto"
	"ps2daemon/utils"
	"sync"

	"gitlab.com/asvedr/giter"
)

type start_stop struct {
	mtx        sync.Mutex
	alive      []entities.ProcFullInfo
	stop_calls []string
	stop_resps map[string]error
	statuses   map[string]entities.ProcessStatus
}

type info_collector struct{ sys_info entities.SysInfo }

type setup struct {
	start_stop     *start_stop
	info_collector *info_collector
	alert_bus      utils.Bus[ps2types.Alert]
	events_bus     utils.Bus[entities.EventCtx]
	killer         proto.IStepProcess
}

func (self *info_collector) GetSysInfo() entities.SysInfo {
	return self.sys_info
}

func (self *info_collector) GetProcInfo(int64) entities.ProcExtInfo { panic("") }

func (self *start_stop) GetAliveServices() []entities.ProcFullInfo {
	return self.alive
}

func (self *start_stop) StopService(key string) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.stop_calls = append(self.stop_calls, key)
	val, found := self.stop_resps[key]
	if !found {
		return nil
	}
	return val
}

func (self *start_stop) GetStatuses() map[string]entities.ProcessStatus {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	return self.statuses
}

func (self *start_stop) StartService(key string) error                      { panic("") }
func (self *start_stop) RestartService(key string) error                    { panic("") }
func (self *start_stop) GetInfo(key string) (entities.ProcFullInfo, error)  { panic("") }
func (self *start_stop) GetLogs(key string, max_rows int) ([]string, error) { panic("") }
func (self *start_stop) GetPid(key string) (int64, error)                   { panic("") }
func (self *start_stop) DoAutostart()                                       { panic("") }

func make_setup() setup {
	config := ps2config_parser.New().GenSample()
	rules, err := giter.MapErr(
		[]string{"proc_mem > 10", "temp > 30"},
		ps2rule.NewParser().ParseProc,
	)
	if err != nil {
		panic(err)
	}
	config.Services[0].KillOn = rules
	start_stop := &start_stop{
		stop_resps: map[string]error{},
	}
	info_collector := &info_collector{}
	alert_bus := utils.NewBus[ps2types.Alert](config)
	events_bus := utils.NewBus[entities.EventCtx](config)
	return setup{
		start_stop:     start_stop,
		info_collector: info_collector,
		alert_bus:      alert_bus,
		events_bus:     events_bus,
		killer: killer.New(
			config,
			start_stop,
			info_collector,
			ps2rule.NewExecutor(),
			alert_bus,
			events_bus,
		),
	}
}
