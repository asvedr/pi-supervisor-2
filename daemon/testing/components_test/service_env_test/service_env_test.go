package service_env_test

import (
	"ps2base/ps2types"
	"ps2daemon/components/env_manager"
	"ps2daemon/components/service"
	"ps2daemon/proto"
	"testing"
	"time"
)

func setup() proto.IService {
	c := ps2types.Config{}
	c.Daemon.ProcLogLen = 100
	f := service.NewFactory(&c, env_manager.New())
	return f.Produce(
		"/bin/sh",
		[]string{"./script.sh", "PARAM"},
		map[string]string{"A": "Val1", "B": "Val2"},
	)
}

func TestProcEnv(t *testing.T) {
	srv := setup()
	srv.Start()
	time.Sleep(time.Second * 2)
	logs := srv.GetLogs(10)
	exp := "PROC: PARAM-Val1-Val2"
	if !(logs[0] == exp || logs[1] == exp) {
		t.Fatal(logs)
	}
	srv.Stop()
}
