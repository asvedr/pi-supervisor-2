package sys_info_test

import (
	"ps2base/ps2types"
	"ps2daemon/components/info_collector"
	"ps2daemon/proto"
	"testing"
)

func make_setup() proto.IInfoCollector {
	c := ps2types.Config{}
	c.Daemon.SysTempSource = "./temp_source.txt"
	return info_collector.New(&c)
}

func TestGetSysInfo(t *testing.T) {
	setup := make_setup()
	info := setup.GetSysInfo()
	if info.Temp.Err != nil {
		t.Fatal(info.Temp.Err)
	}
	if int(info.Temp.Val) != 41 {
		t.Fatal(info.Temp.Val)
	}
	if info.MemTotal.Err != nil ||
		info.MemFree.Err != nil ||
		info.CpuLoad.Err != nil {
		t.Fatalf(
			"%v|%v|%v",
			info.MemTotal.Err,
			info.MemFree.Err,
			info.CpuLoad.Err,
		)
	}
}
