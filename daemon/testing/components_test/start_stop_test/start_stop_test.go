package start_stop_test

import (
	"ps2base/ps2types"
	"ps2daemon/components/env_manager"
	"ps2daemon/components/info_collector"
	"ps2daemon/components/service"
	"ps2daemon/components/start_stop"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/utils"
	"reflect"
	"testing"
	"time"

	"gitlab.com/asvedr/giter"
)

type setup struct {
	ss   proto.IStartStop
	info proto.IInfoCollector
}

func make_setup(label string) setup {
	config := &ps2types.Config{
		Daemon: ps2types.ConfDaemon{
			ProcLogLen:     10,
			RestartTimeout: time.Second,
		},
		Services: []ps2types.ConfService{
			{
				Name:  "script",
				Prog:  "/bin/sh",
				Start: true,
				Args:  []string{"./script.sh", label},
			},
			{
				Name: "autofail",
				Prog: "/bin/sh",
				Args: []string{"./autofail.sh", label},
			},
		},
	}
	env := env_manager.New()
	factory := service.NewFactory(config, env)
	info := info_collector.New(config)
	alert_bus := utils.NewBus[ps2types.Alert](config)
	ss := start_stop.New(config, factory, info, alert_bus)
	return setup{ss: ss, info: info}
}

func TestStartStopOk(t *testing.T) {
	setup := make_setup("TestStartStopOk")
	err := setup.ss.StartService("script")
	if err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Millisecond * 500)
	pid, err := setup.ss.GetPid("script")
	if err != nil {
		t.Fatal(err)
	}
	info := setup.info.GetProcInfo(pid)
	if info.Alive.Err != nil {
		t.Fatal(info.Alive.Err)
	}
	if !info.Alive.Val {
		t.Fatal("proc is dead")
	}

	err = setup.ss.StopService("script")
	if err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Millisecond * 500)
	info = setup.info.GetProcInfo(pid)
	if info.Alive.Err != nil {
		t.Fatal(info.Alive.Err)
	}
	if info.Alive.Val {
		t.Fatal("proc is alive")
	}
}

func TestRestart(t *testing.T) {
	setup := make_setup("TestRestart")
	err := setup.ss.StartService("script")
	if err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Millisecond * 500)
	pid, err := setup.ss.GetPid("script")
	if err != nil {
		t.Fatal(err)
	}
	info := setup.info.GetProcInfo(pid)
	if info.Alive.Err != nil {
		t.Fatal(info.Alive.Err)
	}
	if !info.Alive.Val {
		t.Fatal("proc is dead")
	}

	err = setup.ss.RestartService("script")
	if err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Millisecond * 500)
	pid, err = setup.ss.GetPid("script")
	if err != nil {
		t.Fatal(err)
	}
	info = setup.info.GetProcInfo(pid)
	if info.Alive.Err != nil {
		t.Fatal(info.Alive.Err)
	}
	if !info.Alive.Val {
		t.Fatal("proc is dead")
	}
	logs, err := setup.ss.GetLogs("script", 10)
	if err != nil {
		t.Fatal(err)
	}
	echos := giter.Filter(
		logs,
		func(s string) bool {
			return s == "PROC: im in script!"
		},
	)
	if len(echos) != 2 {
		t.Fatal(logs)
	}

	setup.ss.StopService("script")
}

func TestLifetimeOnRestart(t *testing.T) {
	setup := make_setup("TestRestart")
	setup.ss.StartService("script")
	time.Sleep(time.Second)
	info, _ := setup.ss.GetInfo("script")
	threshold := time.Millisecond * 200
	if (info.Lifetime - time.Second).Abs() > threshold {
		t.Fatal(info.Lifetime)
	}
	setup.ss.RestartService("script")
	time.Sleep(time.Second)
	info, _ = setup.ss.GetInfo("script")
	if info.Lifetime > time.Second+threshold {
		t.Fatal(info.Lifetime)
	}
	setup.ss.StopService("script")
}

func TestAutoRestart(t *testing.T) {
	setup := make_setup("TestRestart")
	setup.ss.StartService("autofail")
	time.Sleep(time.Millisecond * 900)
	info, _ := setup.ss.GetInfo("autofail")
	threshold := time.Millisecond * 200
	if info.Lifetime > time.Second+threshold {
		t.Fatal(info.Lifetime)
	}
	setup.ss.RestartService("autofail")
	time.Sleep(time.Second)
	info, _ = setup.ss.GetInfo("autofail")
	if info.Lifetime > time.Second+threshold {
		t.Fatal(info.Lifetime)
	}
	setup.ss.StopService("autofail")
}

func TestGetAlive(t *testing.T) {
	setup := make_setup("TestRestart")
	alive := setup.ss.GetAliveServices()
	statuses := setup.ss.GetStatuses()
	if len(alive) != 0 {
		t.Fatal(alive)
	}
	if !reflect.DeepEqual(
		statuses,
		map[string]entities.ProcessStatus{
			"script":   entities.ProcessStatusInactive,
			"autofail": entities.ProcessStatusInactive,
		},
	) {
		t.Fatal(statuses)
	}
	setup.ss.StartService("script")
	time.Sleep(time.Millisecond * 500)
	alive = setup.ss.GetAliveServices()
	info, _ := setup.ss.GetInfo("script")
	info.Lifetime = 0
	alive[0].Lifetime = 0
	if !reflect.DeepEqual(alive, giter.V(info)) {
		t.Fatal(alive)
	}
	setup.ss.StopService("script")
}

func TestAutoStart(t *testing.T) {
	setup := make_setup("TestAutoStart")
	setup.ss.DoAutostart()
	time.Sleep(time.Millisecond * 100)
	statuses := setup.ss.GetStatuses()
	setup.ss.StopService("script")
	if !reflect.DeepEqual(
		statuses,
		map[string]entities.ProcessStatus{
			"script":   entities.ProcessStatusActive,
			"autofail": entities.ProcessStatusInactive,
		},
	) {
		t.Fatal(statuses)
	}
}
