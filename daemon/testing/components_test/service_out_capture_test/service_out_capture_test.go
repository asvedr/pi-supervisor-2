package service_out_capture_test

import (
	"ps2base/ps2types"
	"ps2daemon/components/env_manager"
	"ps2daemon/components/service"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"testing"
	"time"
)

func setup() proto.IService {
	c := ps2types.Config{}
	c.Daemon.ProcLogLen = 100
	f := service.NewFactory(&c, env_manager.New())
	return f.Produce(
		"/bin/sh",
		[]string{"./script.sh"},
		map[string]string{},
	)
}

func TestCapture(t *testing.T) {
	srv := setup()
	if srv.GetStatus() != entities.ProcessStatusInactive {
		t.Fatal(srv.GetStatus())
	}
	pid, tm := srv.GetPidAndLifetime()
	if pid != -1 || tm != time.Duration(0) {
		t.Fatalf("%v|%v", pid, tm)
	}
	err := srv.Start()
	if err != nil {
		t.Fatal(err)
	}
	if srv.GetStatus() != entities.ProcessStatusActive {
		srv.Stop()
		t.Fatal(srv.GetStatus())
	}
	time.Sleep(time.Second)
	pid, tm = srv.GetPidAndLifetime()
	if pid == -1 || tm < time.Second || tm > time.Second*2 {
		t.Fatalf("%v|%v", pid, tm)
	}
	logs := srv.GetLogs(10)
	if len(logs) == 0 {
		srv.Stop()
		t.Fatal(logs)
	}
	if logs[len(logs)-1] != "PROC: hello" {
		srv.Stop()
		t.Fatalf("'%s'\n", logs[len(logs)-1])
	}
	err = srv.Stop()
	if err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Second * 2)
	pid, tm = srv.GetPidAndLifetime()
	if pid != -1 || tm != time.Duration(0) {
		t.Fatalf("%v|%v", pid, tm)
	}
	if srv.GetStatus() != entities.ProcessStatusInactive {
		t.Fatal(srv.GetStatus())
	}
}
