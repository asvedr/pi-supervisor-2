package get_alerts_test

import (
	"ps2base/ps2config_parser"
	"ps2base/ps2types"
	"ps2daemon/api/handlers/get_alerts"
	"ps2daemon/proto"
	"ps2daemon/utils"
	"testing"
)

func TestGetAlertsNone(t *testing.T) {
	bus := utils.NewBus[ps2types.Alert](ps2config_parser.New().GenSample())
	handler := get_alerts.New(bus)
	resp, err := proto.JsonHandle(handler, `{"limit": 10}`)
	if err != nil {
		t.Fatal(err)
	}
	if resp != `{"alerts":[]}` {
		t.Fatal(resp)
	}
}

func TestGetAlertsValue(t *testing.T) {
	bus := utils.NewBus[ps2types.Alert](ps2config_parser.New().GenSample())
	handler := get_alerts.New(bus)
	bus.Put(ps2types.Alert{Name: "a", Msg: "1"})
	bus.Put(ps2types.Alert{Name: "b", Msg: "2"})

	resp, err := proto.JsonHandle(handler, `{"limit": 10}`)
	if err != nil {
		t.Fatal(err)
	}
	if resp != `{"alerts":[{"name":"a","msg":"1","type":0},{"name":"b","msg":"2","type":0}]}` {
		t.Fatal(resp)
	}

	resp, err = proto.JsonHandle(handler, `{"limit": 10}`)
	if err != nil {
		t.Fatal(err)
	}
	if resp != `{"alerts":[]}` {
		t.Fatal(resp)
	}
}
