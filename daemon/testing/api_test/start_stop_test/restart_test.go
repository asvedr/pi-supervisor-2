package start_stop_test

import (
	"ps2daemon/api/handlers/restart"
	"ps2daemon/proto"
	"reflect"
	"testing"
)

func TestRestart(t *testing.T) {
	ss := &start_stop{}
	h := restart.New(ss)
	resp, err := proto.JsonHandle(h, `{"service": "abc"}`)
	if err != nil {
		t.Fatal(err)
	}
	if resp != "ok" {
		t.Fatal(resp)
	}
	if !reflect.DeepEqual(ss.restarted, []string{"abc"}) {
		t.Fatal(ss.restarted)
	}
}
