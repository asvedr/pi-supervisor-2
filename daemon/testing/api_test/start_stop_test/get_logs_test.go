package start_stop_test

import (
	"ps2daemon/api/handlers/get_logs"
	"ps2daemon/proto"
	"testing"
)

func TestGetLogs(t *testing.T) {
	ss := &start_stop{}
	h := get_logs.New(ss)
	resp, err := proto.JsonHandle(h, `{"service":"def","max_rows":3}`)
	if err != nil {
		t.Fatal(err)
	}
	exp := "def: 0\ndef: 1\ndef: 2"
	if resp != exp {
		t.Fatalf("\n> %s\n> %s\n", resp, exp)
	}
}
