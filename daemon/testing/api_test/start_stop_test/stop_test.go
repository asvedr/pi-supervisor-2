package start_stop_test

import (
	"ps2daemon/api/handlers/stop"
	"ps2daemon/proto"
	"reflect"
	"testing"
)

func TestStop(t *testing.T) {
	ss := &start_stop{}
	h := stop.New(ss)
	resp, err := proto.JsonHandle(h, `{"service": "abc"}`)
	if err != nil {
		t.Fatal(err)
	}
	if resp != "ok" {
		t.Fatal(resp)
	}
	if !reflect.DeepEqual(ss.stopped, []string{"abc"}) {
		t.Fatal(ss.stopped)
	}
}
