package start_stop_test

import (
	"ps2daemon/api/handlers/start"
	"ps2daemon/proto"
	"reflect"
	"testing"
)

func TestStart(t *testing.T) {
	ss := &start_stop{}
	h := start.New(ss)
	resp, err := proto.JsonHandle(h, `{"service": "abc"}`)
	if err != nil {
		t.Fatal(err)
	}
	if resp != "ok" {
		t.Fatal(resp)
	}
	if !reflect.DeepEqual(ss.started, []string{"abc"}) {
		t.Fatal(ss.started)
	}
}
