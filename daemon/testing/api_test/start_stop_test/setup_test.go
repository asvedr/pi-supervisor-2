package start_stop_test

import (
	"errors"
	"fmt"
	"ps2daemon/entities"
	"time"
)

type start_stop struct {
	started   []string
	stopped   []string
	restarted []string
	get_pid   []string
}

func (self *start_stop) StartService(key string) error {
	self.started = append(self.started, key)
	return nil
}

func (self *start_stop) StopService(key string) error {
	self.stopped = append(self.stopped, key)
	return nil
}

func (self *start_stop) RestartService(key string) error {
	self.restarted = append(self.restarted, key)
	return nil
}

func (self *start_stop) GetPid(key string) (int64, error) {
	self.get_pid = append(self.get_pid, key)
	return 123, nil
}

func (self *start_stop) GetAliveServices() []entities.ProcFullInfo {
	err := errors.New("oops")
	return []entities.ProcFullInfo{
		{
			ProcExtInfo: entities.ProcExtInfo{
				MemUsed: entities.Result[uint64]{Err: err},
				Alive:   entities.Result[bool]{Val: true},
			},
			Service:  "srv",
			Pid:      123,
			Lifetime: time.Second * 2,
		},
	}
}

func (self *start_stop) GetInfo(key string) (entities.ProcFullInfo, error) {
	return entities.ProcFullInfo{
		ProcExtInfo: entities.ProcExtInfo{
			MemUsed: entities.Result[uint64]{Val: 3210},
			Alive:   entities.Result[bool]{Val: true},
		},
		Service:  key,
		Pid:      123,
		Lifetime: time.Second * 2,
	}, nil
}

func (self *start_stop) GetStatuses() map[string]entities.ProcessStatus {
	return map[string]entities.ProcessStatus{
		"a": entities.ProcessStatusStarting,
		"b": entities.ProcessStatusInactive,
	}
}

func (self *start_stop) DoAutostart() { panic("") }

func (self *start_stop) GetLogs(key string, max_rows int) ([]string, error) {
	rows := []string{}
	for i := 0; i < max_rows; i += 1 {
		rows = append(rows, fmt.Sprintf("%s: %d", key, i))
	}
	return rows, nil
}
