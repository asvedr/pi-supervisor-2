package start_stop_test

import (
	"ps2daemon/api/handlers/get_statuses"
	"ps2daemon/proto"
	"testing"
)

func TestGetStatuses(t *testing.T) {
	ss := &start_stop{}
	h := get_statuses.New(ss)
	resp, err := proto.JsonHandle(h, "")
	if err != nil {
		t.Fatal(err)
	}
	exp := `{"a":"starting","b":"inactive"}`
	if resp != exp {
		t.Fatalf("\n> %s\n> %s\n", resp, exp)
	}
}
