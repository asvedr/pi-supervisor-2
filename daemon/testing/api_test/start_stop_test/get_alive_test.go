package start_stop_test

import (
	"ps2daemon/api/handlers/get_alive"
	"ps2daemon/proto"
	"testing"
)

func TestGetAlive(t *testing.T) {
	ss := &start_stop{}
	h := get_alive.New(ss)
	resp, err := proto.JsonHandle(h, "")
	if err != nil {
		t.Fatal(err)
	}
	exp := `{"services":[{"service":"srv","mem":"ERR: oops","pid":123,"lifetime":"2s"}]}`
	if resp != exp {
		t.Fatalf("\n> %s\n> %s\n", resp, exp)
	}
}
