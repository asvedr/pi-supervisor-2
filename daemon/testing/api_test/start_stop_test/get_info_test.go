package start_stop_test

import (
	"ps2daemon/api/handlers/get_info"
	"ps2daemon/proto"
	"testing"
)

func TestGetInfo(t *testing.T) {
	ss := &start_stop{}
	h := get_info.New(ss)
	resp, err := proto.JsonHandle(h, `{"service": "abc"}`)
	if err != nil {
		t.Fatal(err)
	}
	exp := `{"service":"abc","mem_used":"3.21kb","alive":"true","pid":"123","lifetime":"2s"}`
	if resp != exp {
		t.Fatalf("\n> %s\n> %s\n", resp, exp)
	}
}
