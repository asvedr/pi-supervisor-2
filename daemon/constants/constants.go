package constants

const MemKB uint64 = 1000
const MemMB uint64 = MemKB * 1000
const MemGB uint64 = MemMB * 1000
