package api_server

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/pserrors"
)

type server struct {
	srv  *http.ServeMux
	port uint16
}

func New(
	config *ps2types.Config,
	handlers ...proto.IApiHandler,
) proto.IProcess {
	srv := http.NewServeMux()
	hh := help_handler{handlers: handlers}
	for _, h := range append(handlers, hh) {
		srv.HandleFunc(h.Schema().Path, wrap_func(h))
	}
	return server{srv: srv, port: config.Api.Port}
}

func (server) Name() string {
	return "ApiServer"
}

func (self server) Run() error {
	addr := fmt.Sprintf("localhost:%d", self.port)
	log.Printf("api server started on %s", addr)
	err := http.ListenAndServe(addr, self.srv)
	if err != nil {
		log.Printf("server failed with: %v", err)
	}
	return err
}

func wrap_func(h proto.IApiHandler) func(http.ResponseWriter, *http.Request) {
	schema := h.Schema()
	resp_ser := make_resp_serializer(schema.ResponseModel)
	return func(resp http.ResponseWriter, req *http.Request) {
		parsed_req, err := parse_request(schema, req)
		if err != nil {
			write_err(resp, err)
			return
		}
		val, err := h.Handle(parsed_req)
		if err != nil {
			write_err(resp, err)
			return
		}
		write_resp(resp, resp_ser, 200, val, val)
	}
}

func parse_request(schema entities.ApiHandlerSchema, req *http.Request) (any, error) {
	if schema.RequestUnmarshall == nil {
		return nil, nil
	}
	bts, err := io.ReadAll(req.Body)
	if err != nil {
		return nil, fmt.Errorf("can not read request: %v", err)
	}
	return schema.RequestUnmarshall(bts)
}

type ser_err struct {
	Code    string `json:"code"`
	Details string `json:"details"`
}

func write_err(resp http.ResponseWriter, err error) {
	api_err, casted := err.(*pserrors.ErrApi)
	var se ser_err
	status := 500
	if casted {
		se = ser_err{Code: api_err.Code, Details: api_err.Details}
		status = api_err.Status
	} else {
		se = ser_err{Code: "other", Details: err.Error()}
	}
	write_resp(resp, json.Marshal, status, se, api_err)
}

func make_resp_serializer(mdl any) func(any) ([]byte, error) {
	if mdl == nil {
		return ser_bytes
	}
	switch mdl.(type) {
	case []byte:
		return ser_bytes
	case string:
		return ser_string
	default:
		return json.Marshal
	}
}

func ser_bytes(val any) ([]byte, error) {
	bts, casted := val.([]byte)
	if !casted {
		return nil, errors.New("response is not bytes")
	}
	return bts, nil
}

func ser_string(val any) ([]byte, error) {
	str, casted := val.(string)
	if !casted {
		return nil, errors.New("response is not string")
	}
	return []byte(str), nil
}

func write_resp(
	resp http.ResponseWriter,
	ser func(any) ([]byte, error),
	code int,
	val any,
	label any,
) {
	// data, err := json.Marshal(val)
	data, err := ser(val)
	if err != nil {
		log.Printf("can not serialize value: %v", label)
		data = []byte{}
		code = 500
	}
	resp.WriteHeader(code)
	_, err = resp.Write(data)
	if err != nil {
		log.Printf("can not write data to response: %v", err)
	}
}
