package api_server

import (
	"encoding/json"
	"fmt"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"strings"

	"gitlab.com/asvedr/giter"
)

type help_handler struct {
	handlers []proto.IApiHandler
}

func (help_handler) Schema() entities.ApiHandlerSchema {
	return entities.ApiHandlerSchema{
		Path:          "/help/",
		ResponseModel: "",
	}
}

func (self help_handler) Handle(_ any) (any, error) {
	lines := giter.Map(
		self.handlers,
		func(h proto.IApiHandler) string { return self.prepare_one(h) },
	)
	return strings.Join(lines, "\n\n") + "\n", nil
}

func (help_handler) prepare_one(h proto.IApiHandler) string {
	schema := h.Schema()
	result := "- " + schema.Path
	if schema.RequestModel != nil {
		bts, err := json.Marshal(schema.RequestModel)
		if err != nil {
			return fmt.Sprintf("- %s INVALID: %v", schema.Path, err)
		}
		result += "\n  req: " + string(bts)
	}
	if schema.ResponseModel != nil {
		bts, err := json.Marshal(schema.ResponseModel)
		if err != nil {
			return fmt.Sprintf("- %s INVALID: %v", schema.Path, err)
		}
		result += "\n  resp: " + string(bts)
	}
	return result
}
