package killer

import (
	"log"
	"ps2base/ps2proto"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/utils"
	"time"
)

type killer struct {
	config         *ps2types.Config
	start_stop     proto.IStartStop
	info_collector proto.IInfoCollector
	rule_executor  ps2proto.IRuleExecutor
	alert_bus      utils.Bus[ps2types.Alert]
	events_bus     utils.Bus[entities.EventCtx]

	step_timeout       time.Duration
	service_to_reasons map[string][]ps2types.Rule
}

func New(
	config *ps2types.Config,
	start_stop proto.IStartStop,
	info_collector proto.IInfoCollector,
	rule_executor ps2proto.IRuleExecutor,
	alert_bus utils.Bus[ps2types.Alert],
	events_bus utils.Bus[entities.EventCtx],
) proto.IStepProcess {
	service_to_reasons := map[string][]ps2types.Rule{}
	for _, service := range config.Services {
		if len(service.KillOn) > 0 {
			service_to_reasons[service.Name] = service.KillOn
		}
	}
	return &killer{
		config:             config,
		start_stop:         start_stop,
		info_collector:     info_collector,
		service_to_reasons: service_to_reasons,
		rule_executor:      rule_executor,
		alert_bus:          alert_bus,
		events_bus:         events_bus,
		step_timeout:       config.Daemon.KillerTimeout,
	}
}

func (*killer) Name() string {
	return "killer"
}

func (self *killer) DoStep() time.Duration {
	if len(self.service_to_reasons) == 0 {
		return self.step_timeout
	}
	sys_info := self.info_collector.GetSysInfo()
	services := self.start_stop.GetAliveServices()
	statuses := self.start_stop.GetStatuses()
	for _, srv_info := range services {
		if !statuses[srv_info.Service].CanKillFromStatus() {
			continue
		}
		reasons, found := self.service_to_reasons[srv_info.Service]
		if !found {
			continue
		}
		reason := self.find_kill_reason(reasons, sys_info, srv_info)
		if len(reason) > 0 {
			self.kill_service(srv_info.Service, reason)
		}
	}
	return self.step_timeout
}

func (self *killer) kill_service(service string, reason string) {
	err := self.start_stop.StopService(service)
	if err != nil {
		log.Printf("can not stop service(%s): %v", service, err)
		self.alert_bus.Put(ps2types.Alert{
			Name: "can not stop service " + service,
			Msg:  err.Error(),
			Type: ps2types.AlertTypeFail,
		})
		return
	}
	self.events_bus.Put(entities.EventCtx{
		Event: ps2types.EventProcKilled,
		Ctx:   service + ": " + reason,
	})
}

func (self *killer) find_kill_reason(
	rules []ps2types.Rule,
	sys_info entities.SysInfo,
	srv_info entities.ProcFullInfo,
) string {
	failed_vars := append(
		sys_info.FailedVars(),
		srv_info.FailedVars()...,
	)
	for _, rule := range rules {
		if !is_rule_applicable(rule, failed_vars) {
			continue
		}
		self.rule_executor.LocalCtx(
			sys_info.Temp.Val,
			int64(sys_info.MemFree.Val),
			sys_info.CpuLoad.Val,
			int64(srv_info.MemUsed.Val),
		)
		if self.rule_executor.Execute(rule.Expr) {
			return rule.Raw
		}
	}
	return ""
}

func is_rule_applicable(rule ps2types.Rule, failed_vars []string) bool {
	for _, fvar := range failed_vars {
		_, found := rule.Consts[fvar]
		if found {
			return false
		}
	}
	return true
}
