package sys_info_cache

import (
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"sync"
	"time"
)

type Cache struct {
	mtx       sync.RWMutex
	collector proto.IInfoCollector
	cached    entities.SysInfo
	timeout   time.Duration
}

func New(
	config *ps2types.Config,
	collector proto.IInfoCollector,
) *Cache {
	return &Cache{
		collector: collector,
		timeout:   config.Daemon.SysCacheTimeout,
	}
}

func (self *Cache) AsProcess() proto.IProcess {
	return proto.StepProcess{Proc: self}
}

func (self *Cache) AsCollector() proto.IInfoCollector {
	return self
}

func (self *Cache) Name() string {
	return "SysInfoCache"
}

func (self *Cache) DoStep() time.Duration {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.cached = self.collector.GetSysInfo()
	return self.timeout
}

func (self *Cache) GetSysInfo() entities.SysInfo {
	self.mtx.RLock()
	defer self.mtx.RUnlock()
	return self.cached
}

func (self *Cache) GetProcInfo(pid int64) entities.ProcExtInfo {
	return self.collector.GetProcInfo(pid)
}
