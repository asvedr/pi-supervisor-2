package alerter

import (
	"ps2base/ps2proto"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/utils"
	"time"

	"gitlab.com/asvedr/giter"
)

type alerter struct {
	config         *ps2types.Config
	info_collector proto.IInfoCollector
	rule_executor  ps2proto.IRuleExecutor
	alert_bus      utils.Bus[ps2types.Alert]
	events_bus     utils.Bus[entities.EventCtx]

	step_timeout             time.Duration
	alert_to_last_time_notif map[string]time.Time
	alert_to_active_on       map[string]time.Time
	alert_to_inactive_on     map[string]time.Time
}

func New(
	config *ps2types.Config,
	info_collector proto.IInfoCollector,
	rule_executor ps2proto.IRuleExecutor,
	alert_bus utils.Bus[ps2types.Alert],
	events_bus utils.Bus[entities.EventCtx],
) proto.IStepProcess {
	events_bus.Put(entities.EventCtx{Event: ps2types.EventStarted})
	return &alerter{
		config:         config,
		info_collector: info_collector,
		rule_executor:  rule_executor,
		alert_bus:      alert_bus,
		events_bus:     events_bus,

		step_timeout:             config.Daemon.AlerterTimeout,
		alert_to_last_time_notif: map[string]time.Time{},
		alert_to_active_on:       map[string]time.Time{},
		alert_to_inactive_on:     map[string]time.Time{},
	}
}

func (*alerter) Name() string {
	return "alerter"
}

func (self *alerter) DoStep() time.Duration {
	sys_info := self.info_collector.GetSysInfo()
	events := giter.V2M(
		self.events_bus.ReadAll(),
		entities.UnpackEventCtx,
	)
	self.rule_executor.GlobalCtx(
		sys_info.Temp.Val,
		int64(sys_info.MemFree.Val),
		sys_info.CpuLoad.Val,
	)
	failed_vars := sys_info.FailedVars()
	for _, alert := range self.config.Alerts {
		if alert.Event != nil {
			self.process_event_alert(alert, events)
		} else if is_applicable_rule(alert, failed_vars) {
			self.process_rule_alert(alert)
		}
	}
	return self.step_timeout
}

func (self *alerter) process_event_alert(
	cnf ps2types.ConfAlert,
	events map[ps2types.Event]string,
) {
	msg, found := events[*cnf.Event]
	if !found {
		return
	}
	now := time.Now()
	if self.time_since_last_notif(now, cnf.Name) < cnf.RepeatTimeout {
		return
	}
	self.alert_bus.Put(ps2types.Alert{
		Name: cnf.Name,
		Msg:  msg,
		Type: ps2types.AlertTypeNotif,
	})
	self.alert_to_last_time_notif[cnf.Name] = now
}

func (self *alerter) process_rule_alert(alert ps2types.ConfAlert) {
	if self.rule_executor.Execute(alert.Rule.Expr) {
		self.on_active_rule_alert(alert)
	} else {
		self.on_inactive_rule_alert(alert)
	}
}

func (self *alerter) on_inactive_rule_alert(cnf ps2types.ConfAlert) {
	_, found := self.alert_to_active_on[cnf.Name]
	if found {
		delete(self.alert_to_active_on, cnf.Name)
		self.alert_to_inactive_on[cnf.Name] = time.Now()
		return
	}
	ok_time, found := self.alert_to_inactive_on[cnf.Name]
	if !found {
		return
	}
	if time.Now().Sub(ok_time) < cnf.ThresholdTimeout {
		return
	}
	self.alert_bus.Put(ps2types.Alert{
		Name: cnf.Name,
		Msg:  cnf.Rule.Raw,
		Type: ps2types.AlertTypeOk,
	})
	delete(self.alert_to_inactive_on, cnf.Name)
}

func (self *alerter) on_active_rule_alert(cnf ps2types.ConfAlert) {
	now := time.Now()
	delete(self.alert_to_inactive_on, cnf.Name)
	started_at, found := self.alert_to_active_on[cnf.Name]
	if !found {
		self.alert_to_active_on[cnf.Name] = now
		return
	}
	if now.Sub(started_at) < cnf.ThresholdTimeout {
		return
	}
	if self.time_since_last_notif(now, cnf.Name) < cnf.RepeatTimeout {
		return
	}
	self.alert_bus.Put(ps2types.Alert{
		Name: cnf.Name,
		Msg:  cnf.Rule.Raw,
		Type: ps2types.AlertTypeFail,
	})
	self.alert_to_last_time_notif[cnf.Name] = now
}

func (self *alerter) time_since_last_notif(now time.Time, name string) time.Duration {
	notified, found := self.alert_to_last_time_notif[name]
	if !found {
		return now.Sub(time.Unix(0, 0))
	}
	return now.Sub(notified)
}

func is_applicable_rule(alert ps2types.ConfAlert, failed []string) bool {
	for _, v := range failed {
		_, found := alert.Rule.Consts[v]
		if found {
			return false
		}
	}
	return true
}
