package get_alive

import (
	"ps2base/ps2consts"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/utils"
	"time"

	"gitlab.com/asvedr/giter"
)

type handler struct {
	start_stop proto.IStartStop
}

func New(
	start_stop proto.IStartStop,
) proto.IApiHandler {
	return handler{start_stop: start_stop}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaNoRequest(
		ps2consts.GetAliveUrl,
		ps2types.ApiGetAliveResponse{
			Services: []ps2types.ApiService{
				{
					Mem:      "mem or err",
					Pid:      1,
					Lifetime: time.Second.String(),
				},
			},
		},
	)
}

func (self handler) Handle(_ any) (any, error) {
	services := giter.Map(self.start_stop.GetAliveServices(), serialize)
	return ps2types.ApiGetAliveResponse{Services: services}, nil
}

func serialize(info entities.ProcFullInfo) ps2types.ApiService {
	return ps2types.ApiService{
		Service:  info.Service,
		Mem:      entities.StringResult(entities.MapResult(info.MemUsed, utils.SerMem)),
		Pid:      info.Pid,
		Lifetime: info.Lifetime.String(),
	}
}
