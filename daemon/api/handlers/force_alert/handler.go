package force_alert

import (
	"ps2base/ps2consts"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/utils"
)

type handler struct {
	bus utils.Bus[ps2types.Alert]
}

func New(bus utils.Bus[ps2types.Alert]) proto.IApiHandler {
	return handler{bus: bus}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaWithRequest(
		ps2consts.ForceAlertUrl,
		ps2types.Alert{
			Name: "name",
			Msg:  "msg",
			Type: ps2types.AlertTypeNotif,
		},
		"ok",
	)
}

func (self handler) Handle(a_alert any) (any, error) {
	alert := a_alert.(ps2types.Alert)
	self.bus.Put(alert)
	return "ok", nil
}
