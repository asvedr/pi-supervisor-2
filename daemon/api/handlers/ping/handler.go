package ping

import (
	"ps2daemon/entities"
	"ps2daemon/proto"
)

type handler struct{}

const resp = "pong"

func New() proto.IApiHandler {
	return handler{}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaNoRequest(
		"/ping/",
		resp,
	)
}

func (handler) Handle(_ any) (any, error) {
	return resp, nil
}
