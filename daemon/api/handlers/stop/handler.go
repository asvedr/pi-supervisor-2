package stop

import (
	"ps2base/ps2consts"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
)

type handler struct {
	start_stop proto.IStartStop
}

func New(start_stop proto.IStartStop) proto.IApiHandler {
	return handler{start_stop: start_stop}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaWithRequest(
		ps2consts.StopUrl,
		ps2types.ApiServiceAction{Service: "service-name"},
		"ok",
	)
}

func (self handler) Handle(req any) (any, error) {
	request := req.(ps2types.ApiServiceAction)
	err := self.start_stop.StopService(request.Service)
	return "ok", err
}
