package sys_info

import (
	"ps2base/ps2consts"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
)

type handler struct {
	collector proto.IInfoCollector
}

func New(collector proto.IInfoCollector) proto.IApiHandler {
	return handler{collector: collector}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaNoRequest(
		ps2consts.GetSysInfoUrl,
		ps2types.ApiSysInfo{
			Temp:     "45",
			MemFree:  "1gb",
			MemTotal: "2gb",
			CpuLoad:  "100",
		},
	)
}

func (self handler) Handle(_ any) (any, error) {
	info := self.collector.GetSysInfo()
	result := ps2types.ApiSysInfo{
		Temp:     entities.StringResult(info.Temp),
		MemFree:  entities.StringResult(info.MemFree),
		MemTotal: entities.StringResult(info.MemTotal),
		CpuLoad:  entities.StringResult(info.CpuLoad),
	}
	return result, nil
}
