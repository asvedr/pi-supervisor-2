package get_logs

import (
	"ps2base/ps2consts"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"strings"
)

type handler struct {
	start_stop proto.IStartStop
}

func New(
	start_stop proto.IStartStop,
) proto.IApiHandler {
	return handler{start_stop: start_stop}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaWithRequest(
		ps2consts.GetLogsUrl,
		ps2types.ApiGetLogsRequest{Service: "service-name"},
		"logs",
	)
}

func (self handler) Handle(req any) (any, error) {
	request := req.(ps2types.ApiGetLogsRequest)
	rows, err := self.start_stop.GetLogs(request.Service, request.MaxRows)
	if err != nil {
		return nil, err
	}
	return strings.Join(rows, "\n"), nil
}
