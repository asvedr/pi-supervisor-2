package get_info

import (
	"fmt"
	"ps2base/ps2consts"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/utils"
	"time"
)

type handler struct {
	start_stop proto.IStartStop
}

func New(
	start_stop proto.IStartStop,
) proto.IApiHandler {
	return handler{start_stop: start_stop}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaWithRequest(
		ps2consts.GetInfoUrl,
		ps2types.ApiServiceAction{Service: "service-name"},
		ps2types.ApiInfo{
			Service:  "name",
			MemUsed:  "bytes or err",
			Alive:    "bool or err",
			Pid:      "int or 'N/A'",
			Lifetime: time.Second.String(),
		},
	)
}

func (self handler) Handle(req any) (any, error) {
	request := req.(ps2types.ApiServiceAction)
	info, err := self.start_stop.GetInfo(request.Service)
	if err != nil {
		return nil, err
	}
	var pid string
	if info.Pid < 0 {
		pid = "N/A"
	} else {
		pid = fmt.Sprint(info.Pid)
	}
	resp := ps2types.ApiInfo{
		Service:  info.Service,
		MemUsed:  entities.StringResult(entities.MapResult(info.MemUsed, utils.SerMem)),
		Alive:    entities.StringResult(info.Alive),
		Pid:      pid,
		Lifetime: info.Lifetime.String(),
	}
	return resp, nil
}
