package get_alerts

import (
	"fmt"
	"ps2base/ps2consts"
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/utils"
)

type handler struct {
	alert_bus utils.Bus[ps2types.Alert]
}

func New(alert_bus utils.Bus[ps2types.Alert]) proto.IApiHandler {
	return handler{alert_bus: alert_bus}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaWithRequest(
		ps2consts.GetAlertsUrl,
		ps2types.ApiGetAlertsRequest{Limit: 10},
		ps2types.ApiGetAlertsResponse{
			[]ps2types.Alert{{Name: "name", Msg: "msg", Type: ps2types.AlertTypeFail}},
		},
	)
}

func (self handler) Handle(req any) (any, error) {
	request, casted := req.(ps2types.ApiGetAlertsRequest)
	if !casted {
		panic(fmt.Sprintf("not casted: %v", req))
	}
	limit := request.Limit
	alerts := self.alert_bus.ReadLimit(limit)
	return ps2types.ApiGetAlertsResponse{Alerts: alerts}, nil
}
