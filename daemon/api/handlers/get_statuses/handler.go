package get_statuses

import (
	"ps2base/ps2consts"
	"ps2daemon/entities"
	"ps2daemon/proto"

	"gitlab.com/asvedr/giter"
)

type handler struct {
	start_stop proto.IStartStop
}

func New(
	start_stop proto.IStartStop,
) proto.IApiHandler {
	return handler{start_stop: start_stop}
}

func (handler) Schema() entities.ApiHandlerSchema {
	return entities.MakeApiHandlerSchemaNoRequest(
		ps2consts.GetStatusesUrl,
		map[string]string{"service": "status"},
	)
}

func (self handler) Handle(_ any) (any, error) {
	f := func(name string, stat entities.ProcessStatus) (string, string) {
		return name, stat.String()
	}
	return giter.M2M(self.start_stop.GetStatuses(), f), nil
}
