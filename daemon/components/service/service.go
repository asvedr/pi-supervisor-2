package service

import (
	"container/list"
	"fmt"
	"log"
	"os/exec"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/pserrors"
	"strings"
	"sync"
	"time"
)

const (
	max_start_id       uint8 = 100
	proc_prefix              = "PROC: "
	err_prefix               = "ERR: "
	super_prefix             = "SUPERVISOR: "
	started_line             = "started"
	terminate_sig_line       = "send terminate signal"
	terminated_line          = "terminated"
)

type log_wrapper struct {
	mtx         *sync.Mutex
	logs        *list.List
	max_log_len int
}

type service struct {
	log_wrapper
	prog       string
	args       []string
	env        map[string]string
	env_man    proto.IEnvManager
	cmd        *exec.Cmd
	started_at time.Time
	start_id   uint8
	status     entities.ProcessStatus
}

func new_service(
	env_man proto.IEnvManager,
	prog string,
	args []string,
	env map[string]string,
	max_log_len int,
) proto.IService {
	var mtx sync.Mutex
	return &service{
		log_wrapper: log_wrapper{
			mtx:         &mtx,
			logs:        list.New(),
			max_log_len: max_log_len,
		},
		prog:    prog,
		args:    args,
		env:     env,
		env_man: env_man,
		status:  entities.ProcessStatusInactive,
	}
}

func (self *service) Start() error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if self.status != entities.ProcessStatusInactive {
		return pserrors.ErrProcInvalidState{
			Msg: fmt.Sprintf(
				"process is not stopped(%v)",
				self.status,
			),
		}
	}
	if self.cmd != nil {
		panic("self.cmd != nil")
	}
	self.status = entities.ProcessStatusStarting
	err := self.launch()
	if err != nil {
		self.add_log(err_prefix, err.Error())
		return err
	}
	self.start_id = (self.start_id + 1) % max_start_id
	cmd := self.cmd
	start_id := self.start_id
	go self.watchdog(cmd, start_id)
	return nil
}

func (self *service) Stop() error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if self.status != entities.ProcessStatusActive {
		return pserrors.ErrProcInvalidState{
			Msg: fmt.Sprintf(
				"process is not active(%v)",
				self.status,
			),
		}
	}
	if self.cmd == nil {
		panic("self.cmd == nil")
	}
	self.status = entities.ProcessStatusStopping
	err := self.cmd.Process.Kill()
	self.add_log(super_prefix, terminate_sig_line)
	if err != nil {
		self.add_log(err_prefix, err.Error())
		return pserrors.ErrCanNotKill{Cause: err}
	}
	return nil
}

func (self *service) GetStatus() entities.ProcessStatus {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if self.cmd != nil {
		return entities.ProcessStatusActive
	}
	return entities.ProcessStatusInactive
}

func (self *service) GetLogs(max_len int) []string {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	var result []string
	for item := self.logs.Front(); item != nil; item = item.Next() {
		result = append(result, item.Value.(string))
	}
	return result
}

// -1 if proc is dead
func (self *service) GetPid() int64 {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if self.cmd == nil {
		return -1
	}
	return int64(self.cmd.Process.Pid)
}

func (self *service) GetPidAndLifetime() (int64, time.Duration) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if self.cmd == nil {
		return -1, time.Duration(0)
	}
	pid := int64(self.cmd.Process.Pid)
	dur := time.Now().Sub(self.started_at)
	return pid, dur
}

func (self *service) launch() error {
	f := func() error {
		self.cmd = exec.Command(self.prog, self.args...)
		wrapper := &log_wrapper{
			mtx:         self.mtx,
			logs:        self.logs,
			max_log_len: self.max_log_len,
		}
		self.cmd.Stdout = wrapper
		self.cmd.Stderr = wrapper
		return self.cmd.Start()
	}
	err := self.env_man.WithEnv(self.env, f)
	if err == nil {
		self.status = entities.ProcessStatusActive
		self.started_at = time.Now()
	}
	return err
}

func (self *service) watchdog(cmd *exec.Cmd, start_id uint8) {
	// cmd.CombinedOutput()
	err := cmd.Wait()
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if self.start_id != start_id {
		// process now occuped by a new start
		return
	}
	if err != nil {
		self.add_log(err_prefix, err.Error())
	}
	self.add_log(super_prefix, terminated_line)
	self.cmd = nil
	if !self.status.CanRestartFromStatus() {
		self.status = entities.ProcessStatusInactive
		return
	}
	err = self.launch()
	if err != nil {
		self.status = entities.ProcessStatusInactive
		log.Printf(
			"failed on autorestart(%s): %v",
			self.prog,
			err,
		)
		self.add_log(err_prefix, err.Error())
		return
	}
	cmd = self.cmd
	go self.watchdog(cmd, start_id)
}

func (self *log_wrapper) add_log(prefix string, line string) {
	if self.logs.Len() >= self.max_log_len {
		self.logs.Remove(self.logs.Front())
	}
	log := prefix + strings.TrimSpace(line)
	self.logs.PushBack(log)
}

func (self *log_wrapper) Write(val []byte) (int, error) {
	msg := strings.TrimSpace(string(val))
	if len(msg) == 0 {
		return len(val), nil
	}
	self.mtx.Lock()
	defer self.mtx.Unlock()
	for _, line := range strings.Split(msg, "\n") {
		self.add_log(proc_prefix, line)
	}
	return len(val), nil
}
