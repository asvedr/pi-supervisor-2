package service

import (
	"ps2base/ps2types"
	"ps2daemon/proto"
)

type service_factory struct {
	env_man     proto.IEnvManager
	max_log_len int
}

func NewFactory(
	config *ps2types.Config,
	env_man proto.IEnvManager,
) proto.IServiceFactory {
	mll := config.Daemon.ProcLogLen
	return service_factory{env_man: env_man, max_log_len: mll}
}

func (self service_factory) Produce(
	prog string,
	args []string,
	env map[string]string,
) proto.IService {
	return new_service(
		self.env_man,
		prog,
		args,
		env,
		self.max_log_len,
	)
}
