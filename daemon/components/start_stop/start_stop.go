package start_stop

import (
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/pserrors"
	"ps2daemon/utils"
	"time"

	"gitlab.com/asvedr/giter"
)

type start_stop struct {
	services        map[string]proto.IService
	service_order   []string
	restart_timeout time.Duration
	info_collector  proto.IInfoCollector
	alert_bus       utils.Bus[ps2types.Alert]
	auto_start      []string
}

func New(
	config *ps2types.Config,
	factory proto.IServiceFactory,
	info_collector proto.IInfoCollector,
	alert_bus utils.Bus[ps2types.Alert],
) proto.IStartStop {
	services := map[string]proto.IService{}
	var service_order []string
	var auto_start []string
	for _, cnf_srv := range config.Services {
		srv := factory.Produce(cnf_srv.Prog, cnf_srv.Args, cnf_srv.Env)
		services[cnf_srv.Name] = srv
		service_order = append(service_order, cnf_srv.Name)
		if cnf_srv.Start {
			auto_start = append(auto_start, cnf_srv.Name)
		}
	}
	return &start_stop{
		services:        services,
		service_order:   service_order,
		restart_timeout: config.Daemon.RestartTimeout,
		info_collector:  info_collector,
		alert_bus:       utils.NewBus[ps2types.Alert](config),
		auto_start:      auto_start,
	}
}

func (self *start_stop) StartService(key string) error {
	srv, found := self.services[key]
	if !found {
		return pserrors.ErrServiceNotFound{}
	}
	return srv.Start()
}

func (self *start_stop) StopService(key string) error {
	srv, found := self.services[key]
	if !found {
		return pserrors.ErrServiceNotFound{}
	}
	return srv.Stop()
}

func (self *start_stop) RestartService(key string) error {
	srv, found := self.services[key]
	if !found {
		return pserrors.ErrServiceNotFound{}
	}
	var err error
	switch srv.GetStatus() {
	case entities.ProcessStatusStarting:
		return nil
	case entities.ProcessStatusActive:
		err = srv.Stop()
	case entities.ProcessStatusStopping:
		err = nil
	case entities.ProcessStatusInactive:
		return srv.Start()
	}
	if err != nil {
		return err
	}
	time.Sleep(self.restart_timeout)
	return srv.Start()
}

func (self *start_stop) GetPid(key string) (int64, error) {
	srv, found := self.services[key]
	if !found {
		return 0, pserrors.ErrServiceNotFound{}
	}
	return srv.GetPid(), nil
}

func (self *start_stop) GetAliveServices() []entities.ProcFullInfo {
	var result []entities.ProcFullInfo
	for name, srv := range self.services {
		if srv.GetStatus() != entities.ProcessStatusActive {
			continue
		}
		result = append(result, self.get_srv_info(name, srv))
	}
	return result
}

func (self *start_stop) GetInfo(key string) (entities.ProcFullInfo, error) {
	srv, found := self.services[key]
	if !found {
		return entities.ProcFullInfo{}, pserrors.ErrServiceNotFound{}
	}
	return self.get_srv_info(key, srv), nil
}

func (self *start_stop) get_srv_info(name string, srv proto.IService) entities.ProcFullInfo {
	pid, lifetime := srv.GetPidAndLifetime()
	collected := self.info_collector.GetProcInfo(pid)
	var info entities.ProcFullInfo
	info.MemUsed = collected.MemUsed
	info.Alive = collected.Alive
	info.Service = name
	info.Pid = pid
	info.Lifetime = lifetime
	return info
}

func (self *start_stop) GetStatuses() map[string]entities.ProcessStatus {
	return giter.M2M(
		self.services,
		func(name string, srv proto.IService) (string, entities.ProcessStatus) {
			return name, srv.GetStatus()
		},
	)
}

func (self *start_stop) GetLogs(key string, max_rows int) ([]string, error) {
	srv, found := self.services[key]
	if !found {
		return nil, pserrors.ErrServiceNotFound{}
	}
	return srv.GetLogs(max_rows), nil
}

func (self *start_stop) DoAutostart() {
	for _, srv := range self.auto_start {
		err := self.StartService(srv)
		if err != nil {
			self.alert_bus.Put(ps2types.Alert{
				Name: "can not autostart " + srv,
				Msg:  err.Error(),
				Type: ps2types.AlertTypeFail,
			})
		}
	}
}
