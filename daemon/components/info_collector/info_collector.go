package info_collector

import (
	"fmt"
	"os"
	"os/exec"
	"ps2base/ps2types"
	"ps2daemon/constants"
	"ps2daemon/entities"
	"ps2daemon/proto"
	"ps2daemon/pserrors"
	"strconv"
	"strings"
)

const sys_mem_source = "/proc/meminfo"
const sys_proc_cmd_name = "top"

var sys_proc_cmd_args = []string{"-i", "-b", "-n", "1"}

var status_map = map[rune]bool{
	'R': true,  //  Running
	'S': true,  //  Sleeping in an interruptible wait
	'D': true,  //  Waiting in uninterruptible disk sleep
	'Z': false, //  Zombie
	'T': false, //  Stopped (on a signal) or (before Linux 2.6.33) trace stopped
	't': false, //  Tracing stop (Linux 2.6.33 onward)
	'X': false, //  Dead (from Linux 2.6.0 onward)
}

var mult_map = map[string]uint64{
	"b":  1,
	"kb": constants.MemKB,
	"mb": constants.MemMB,
	"gb": constants.MemGB,
}

type info_collector struct {
	sys_temp_source string
}

type parsed_mem struct {
	total uint64
	free  uint64
}

func New(config *ps2types.Config) proto.IInfoCollector {
	return info_collector{
		sys_temp_source: config.Daemon.SysTempSource,
	}
}

func (self info_collector) GetSysInfo() entities.SysInfo {
	var info entities.SysInfo
	pm, err := get_mem()
	info.MemFree.Val = pm.free
	info.MemFree.Err = err
	info.MemTotal.Val = pm.total
	info.MemTotal.Err = err
	temp, err := self.get_temp()
	info.Temp.Val = temp
	info.Temp.Err = err
	cpu, err := get_cpu()
	info.CpuLoad.Val = cpu
	info.CpuLoad.Err = err
	return info
}

func (info_collector) GetProcInfo(pid int64) entities.ProcExtInfo {
	path := fmt.Sprintf("/proc/%d/status", pid)
	bts, err := os.ReadFile(path)
	var result = entities.ProcExtInfo{}
	if err != nil {
		result.Alive.Val = false
		result.MemUsed.Val = 0
		return result
	}
	lines := strings.Split(string(bts), "\n")
	alive, err := is_alive(lines)
	result.Alive.Val = alive
	result.Alive.Err = err
	mem, err := get_mem_row_num(lines, "VmRSS")
	result.MemUsed.Val = mem
	result.MemUsed.Err = err
	return result
}

func (self info_collector) get_temp() (float64, error) {
	path := self.sys_temp_source
	f := func(src string) (float64, error) {
		src = strings.TrimSpace(src)
		val, err := strconv.ParseFloat(src, 64)
		return val / 1000.0, err
	}
	return read_parse_file(path, f)
}

func get_row(src []string, key string) (string, error) {
	for _, line := range src {
		if strings.HasPrefix(line, key) {
			return string([]rune(line)[len(key)+1:]), nil
		}
	}
	msg := fmt.Sprintf("Row %s not found", key)
	return "", pserrors.ErrCanNotParseData{Msg: msg}
}

func get_mem_row_num(src []string, key string) (uint64, error) {
	row, err := get_row(src, key) //?.to_lowercase();
	if err != nil {
		return 0, err
	}
	row = strings.ToLower(row)
	split := strings.Split(row, " ")
	if len(split)-2 < 0 {
		return 0, pserrors.ErrCanNotParseData{
			Msg: fmt.Sprintf("invalid line: '%s'", strings.Join(src, " ")),
		}
	}
	mult, found := mult_map[split[len(split)-1]]
	if !found {
		return 0, pserrors.ErrCanNotParseData{
			Msg: "unknown suffix: " + split[len(split)-1],
		}
	}
	val, err := strconv.ParseFloat(split[len(split)-2], 64)
	if err != nil {
		return 0, pserrors.ErrCanNotParseData{
			Msg: "invalid number: " + split[len(split)-2],
		}
	}
	return uint64(val * float64(mult)), nil
}

func is_alive(src []string) (bool, error) {
	row, err := get_row(src, "State")
	if err != nil {
		return false, err
	}
	row_vec := []rune(strings.TrimSpace(row))
	if len(row_vec) == 0 {
		return false, pserrors.ErrCanNotParseData{
			Msg: "state row is empty",
		}
	}
	is_alive, found := status_map[row_vec[0]]
	if !found {
		return false, pserrors.ErrCanNotParseData{
			Msg: "invalid process state: " + string(row_vec[0]),
		}
	}
	return is_alive, nil
}

func parse_mem(src string) (parsed_mem, error) {
	lines := strings.Split(src, "\n")
	total, err_total := get_mem_row_num(lines, "MemTotal")
	free, err_free := get_mem_row_num(lines, "MemFree")
	res := parsed_mem{total: total, free: free}
	if err_total != nil || err_free != nil {
		return res, pserrors.ErrCanNotParseData{
			Msg: "can not parse mem",
		}
	}
	return res, nil
}

func get_mem() (parsed_mem, error) {
	return read_parse_file(sys_mem_source, parse_mem)
}

func get_cpu() (float64, error) {
	cmd := exec.Command(sys_proc_cmd_name, sys_proc_cmd_args...)
	bts, err := cmd.Output()
	if err != nil {
		return 0, pserrors.ErrCanNotGetData{}
	}
	text := string(bts)
	line, err := split_n_get(text, "\n", 2)
	if err != nil {
		return 0, err
	}
	if !strings.HasPrefix(line, "%Cpu(s):") {
		return 0, pserrors.ErrCanNotParseData{
			Msg: "can not find %Cpu row",
		}
	}
	token, err := split_n_get(line, ":", 1)
	if err != nil {
		return 0, err
	}
	split := strings.Split(strings.TrimSpace(token), " ")
	if len(split) < 2 || !strings.HasPrefix(split[1], "us") {
		return 0, pserrors.ErrCanNotParseData{
			Msg: "can not find cpu value",
		}
	}
	return strconv.ParseFloat(split[0], 64)
}

func split_n_get(src string, sep string, num int) (string, error) {
	split := strings.Split(src, sep)
	if len(split) <= num {
		return "", pserrors.ErrCanNotParseData{
			Msg: fmt.Sprintf("invalid line: '%s'", src),
		}
	}
	return split[num], nil
}

func read_parse_file[T any](
	path string,
	parser func(string) (T, error),
) (T, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		var res T
		return res, err
	}
	return parser(string(data))
}
