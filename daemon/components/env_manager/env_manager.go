package env_manager

import (
	"os"
	"ps2daemon/proto"
	"ps2daemon/pserrors"
	"sync"
)

type manager struct {
	mtx sync.Mutex
}

func New() proto.IEnvManager {
	return &manager{}
}

func (self *manager) WithEnv(env map[string]string, action func() error) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if env == nil {
		return action()
	}
	for key, val := range env {
		err := os.Setenv(key, val)
		if err != nil {
			return pserrors.ErrCanNotSetEnv{Cause: err}
		}
	}
	return action()
}
