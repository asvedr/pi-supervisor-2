package proto

import "time"

type IStepProcess interface {
	Name() string
	DoStep() time.Duration
}

type IProcess interface {
	Name() string
	Run() error
}

type StepProcess struct{ Proc IStepProcess }

func (p StepProcess) Name() string { return p.Proc.Name() }
func (p StepProcess) Run() error {
	for {
		time.Sleep(p.Proc.DoStep())
	}
}
