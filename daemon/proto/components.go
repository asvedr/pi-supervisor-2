package proto

import (
	"ps2daemon/entities"
	"time"
)

type IServiceFactory interface {
	Produce(string, []string, map[string]string) IService
}

type IService interface {
	Start() error
	Stop() error
	GetStatus() entities.ProcessStatus
	GetLogs(max_len int) []string
	// pid = -1 if proc is dead
	GetPid() int64
	GetPidAndLifetime() (int64, time.Duration)
}

type IEnvManager interface {
	WithEnv(map[string]string, func() error) error
}

type IInfoCollector interface {
	GetSysInfo() entities.SysInfo
	GetProcInfo(int64) entities.ProcExtInfo
}

type IStartStop interface {
	StartService(key string) error
	StopService(key string) error
	RestartService(key string) error
	GetPid(key string) (int64, error)
	GetAliveServices() []entities.ProcFullInfo
	GetInfo(key string) (entities.ProcFullInfo, error)
	GetStatuses() map[string]entities.ProcessStatus
	GetLogs(key string, max_rows int) ([]string, error)

	DoAutostart()
}
