package proto

import (
	"encoding/json"
	"fmt"
	"ps2daemon/entities"
	"reflect"
)

type IApiHandler interface {
	Schema() entities.ApiHandlerSchema
	Handle(req any) (any, error)
}

func JsonHandle[T IApiHandler](handler T, req_src string) (string, error) {
	schema := handler.Schema()
	var req, resp any
	var err error
	if schema.RequestModel != nil {
		req, err = schema.RequestUnmarshall([]byte(req_src))
		if err != nil {
			return "", err
		}
	}
	fmt.Printf(">> %v\n>> %v\n", schema.RequestModel, req)
	resp, err = handler.Handle(req)
	if err != nil {
		return "", err
	}
	if schema.ResponseModel == nil || reflect.TypeOf(schema.ResponseModel) == reflect.TypeOf([]byte{}) {
		return fmt.Sprint(resp.([]byte)), err
	}
	if reflect.TypeOf(schema.ResponseModel) == reflect.TypeOf("") {
		return resp.(string), nil
	}
	resp_bts, err := json.Marshal(resp)
	return string(resp_bts), err
}
