package pserrors

import "fmt"

type ErrProcInvalidState struct {
	Msg string
}

type ErrCanNotKill struct {
	Cause error
}

type ErrCanNotSetEnv struct {
	Cause error
}

type ErrServiceNotFound struct{}
type ErrCanNotParseData struct {
	Msg string
}
type ErrCanNotGetData struct{}
type ErrApi struct {
	Status  int
	Code    string
	Details string
}

func (e *ErrApi) Error() string {
	return fmt.Sprintf(
		"ErrApi(status=%d, code=%s, details='%s')",
		e.Status,
		e.Code,
		e.Details,
	)
}

func (e ErrProcInvalidState) Error() string {
	return "ErrProcInvalidState: " + e.Msg
}

func (e ErrCanNotKill) Error() string {
	return "ErrCanNotKill: " + e.Cause.Error()
}

func (e ErrCanNotSetEnv) Error() string {
	return "ErrCanNotSetEnv: " + e.Cause.Error()
}

func (e ErrServiceNotFound) Error() string {
	return "ErrServiceNotFound"
}

func (e ErrCanNotParseData) Error() string {
	return "ErrCanNotParseData(" + e.Msg + ")"
}

func (e ErrCanNotGetData) Error() string {
	return "ErrCanNotParseData"
}
