package app

import (
	"ps2base/ps2rule"
	"ps2daemon/processes/alerter"
	"ps2daemon/processes/api_server"
	"ps2daemon/processes/killer"
	"ps2daemon/proto"

	"gitlab.com/asvedr/cldi/di"
)

var SysInfoCacheProcess = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		return InfoCollector.Get().AsProcess()
	},
}

var ServerProcess = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		return api_server.New(
			Config.Get(),
			ApiHandlers.Get()...,
		)
	},
}

var AlerterProcess = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		proc := alerter.New(
			Config.Get(),
			InfoCollector.Get(),
			ps2rule.NewExecutor(),
			AlertBus.Get(),
			EventsBus.Get(),
		)
		return proto.StepProcess{Proc: proc}
	},
}

var KillerProcess = di.Singleton[proto.IProcess]{
	PureFunc: func() proto.IProcess {
		proc := killer.New(
			Config.Get(),
			StartStop.Get(),
			InfoCollector.Get(),
			ps2rule.NewExecutor(),
			AlertBus.Get(),
			EventsBus.Get(),
		)
		return proto.StepProcess{Proc: proc}
	},
}
