package app

import (
	"ps2base/ps2types"
	"ps2daemon/entities"
	"ps2daemon/utils"

	"gitlab.com/asvedr/cldi/di"
)

var EventsBus = di.Singleton[utils.Bus[entities.EventCtx]]{
	PureFunc: func() utils.Bus[entities.EventCtx] {
		return utils.NewBus[entities.EventCtx](Config.Get())
	},
}

var AlertBus = di.Singleton[utils.Bus[ps2types.Alert]]{
	PureFunc: func() utils.Bus[ps2types.Alert] {
		return utils.NewBus[ps2types.Alert](Config.Get())
	},
}
