package app

import (
	"ps2daemon/api/handlers/force_alert"
	"ps2daemon/api/handlers/get_alerts"
	"ps2daemon/api/handlers/get_alive"
	"ps2daemon/api/handlers/get_info"
	"ps2daemon/api/handlers/get_logs"
	"ps2daemon/api/handlers/get_statuses"
	"ps2daemon/api/handlers/ping"
	"ps2daemon/api/handlers/restart"
	"ps2daemon/api/handlers/start"
	"ps2daemon/api/handlers/stop"
	"ps2daemon/api/handlers/sys_info"
	"ps2daemon/proto"

	"gitlab.com/asvedr/cldi/di"
)

var ApiHandlers = di.Singleton[[]proto.IApiHandler]{
	PureFunc: func() []proto.IApiHandler {
		return []proto.IApiHandler{
			ping.New(),
			start.New(StartStop.Get()),
			stop.New(StartStop.Get()),
			restart.New(StartStop.Get()),
			get_statuses.New(StartStop.Get()),
			get_logs.New(StartStop.Get()),
			get_info.New(StartStop.Get()),
			get_alive.New(StartStop.Get()),
			get_alerts.New(AlertBus.Get()),
			force_alert.New(AlertBus.Get()),
			sys_info.New(InfoCollector.Get()),
		}
	},
}
