package app

import (
	"ps2daemon/components/env_manager"
	"ps2daemon/components/info_collector"
	"ps2daemon/components/service"
	"ps2daemon/components/start_stop"
	"ps2daemon/processes/sys_info_cache"
	"ps2daemon/proto"

	"gitlab.com/asvedr/cldi/di"
)

var ServiceFactory = di.Singleton[proto.IServiceFactory]{
	PureFunc: func() proto.IServiceFactory {
		return service.NewFactory(Config.Get(), env_manager.New())
	},
}

var InfoCollector = di.Singleton[*sys_info_cache.Cache]{
	PureFunc: func() *sys_info_cache.Cache {
		return sys_info_cache.New(
			Config.Get(),
			info_collector.New(Config.Get()),
		)
	},
}

var StartStop = di.Singleton[proto.IStartStop]{
	PureFunc: func() proto.IStartStop {
		return start_stop.New(
			Config.Get(),
			ServiceFactory.Get(),
			InfoCollector.Get(),
			AlertBus.Get(),
		)
	},
}
