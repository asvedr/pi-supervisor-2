package app

import (
	"ps2base/ps2config_parser"
	"ps2base/ps2types"

	"gitlab.com/asvedr/cldi/di"
)

var Config = di.Singleton[*ps2types.Config]{
	ErrFunc: func() (*ps2types.Config, error) {
		parser := ps2config_parser.New()
		path := parser.RevealPath()
		return parser.Parse(path)
	},
}
