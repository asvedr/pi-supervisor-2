package entities

import "encoding/json"

type ApiHandlerSchema struct {
	Path              string
	RequestModel      any
	RequestUnmarshall func([]byte) (any, error)
	ResponseModel     any
}

func MakeApiHandlerSchemaNoRequest(
	path string,
	response_model any,
) ApiHandlerSchema {
	return ApiHandlerSchema{
		Path:              path,
		RequestModel:      nil,
		RequestUnmarshall: nil,
		ResponseModel:     response_model,
	}
}

func MakeApiHandlerSchemaWithRequest[R any](
	path string,
	request_model R,
	response_model any,
) ApiHandlerSchema {
	return ApiHandlerSchema{
		Path:              path,
		RequestModel:      request_model,
		RequestUnmarshall: prepare_unmarshall_func[R],
		ResponseModel:     response_model,
	}
}

func prepare_unmarshall_func[T any](bts []byte) (any, error) {
	var mdl T
	err := json.Unmarshal(bts, &mdl)
	return mdl, err
}
