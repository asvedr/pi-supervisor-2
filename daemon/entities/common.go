package entities

import "fmt"

type Result[T any] struct {
	Val T
	Err error
}

func StringResult[T any](r Result[T]) string {
	if r.Err != nil {
		return fmt.Sprintf("ERR: %v", r.Err)
	}
	return fmt.Sprintf("%v", r.Val)
}

func MapResult[A any, B any](r Result[A], f func(A) B) Result[B] {
	if r.Err != nil {
		return Result[B]{Err: r.Err}
	}
	return Result[B]{Val: f(r.Val)}
}
