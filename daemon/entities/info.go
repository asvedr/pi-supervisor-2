package entities

import (
	"ps2base/ps2consts"
	"ps2base/ps2types"
	"time"
)

type SysInfo struct {
	Temp     Result[float64]
	MemTotal Result[uint64]
	MemFree  Result[uint64]
	CpuLoad  Result[float64]
}

type ProcExtInfo struct {
	MemUsed Result[uint64]
	Alive   Result[bool]
}

type ProcFullInfo struct {
	ProcExtInfo
	Service  string
	Pid      int64
	Lifetime time.Duration
}

type EventCtx struct {
	Event ps2types.Event
	Ctx   string
}

func UnpackEventCtx(e EventCtx) (ps2types.Event, string) {
	return e.Event, e.Ctx
}

func (self *ProcFullInfo) FailedVars() []string {
	result := []string{}
	if self.MemUsed.Err != nil {
		result = append(result, ps2consts.RuleConstProcMem)
	}
	return result
}

func (self *SysInfo) FailedVars() []string {
	result := []string{}
	if self.Temp.Err != nil {
		result = append(result, ps2consts.RuleConstTemp)
	}
	// if self.MemTotal.Err != nil
	if self.MemFree.Err != nil {
		result = append(result, ps2consts.RuleConstFreeMem)
	}
	if self.CpuLoad.Err != nil {
		result = append(result, ps2consts.RuleConstCpu)
	}

	return result
}
