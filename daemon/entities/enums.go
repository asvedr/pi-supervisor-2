package entities

import "fmt"

type ProcessStatus int

const (
	ProcessStatusStarting ProcessStatus = iota
	ProcessStatusActive
	ProcessStatusStopping
	ProcessStatusInactive
)

func (s ProcessStatus) CanRestartFromStatus() bool {
	switch s {
	case ProcessStatusStarting:
		return true
	case ProcessStatusActive:
		return true
	case ProcessStatusStopping:
		return false
	case ProcessStatusInactive:
		return false
	}
	panic(fmt.Sprintf("invalid status: %d", int(s)))
}

func (s ProcessStatus) CanKillFromStatus() bool {
	return s == ProcessStatusActive
}

func (s ProcessStatus) String() string {
	switch s {
	case ProcessStatusStarting:
		return "starting"
	case ProcessStatusActive:
		return "active"
	case ProcessStatusStopping:
		return "stopping"
	case ProcessStatusInactive:
		return "inactive"
	}
	panic(fmt.Sprintf("invalid status: %d", int(s)))
}
