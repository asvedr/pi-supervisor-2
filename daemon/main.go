package main

import (
	"os"
	"ps2base/ps2consts"
	"ps2daemon/app"
	"ps2daemon/proto"
	"time"

	"gitlab.com/asvedr/giter"
)

func main() {
	if len(os.Args) > 1 && os.Args[1] == "-h" {
		println("Version: " + ps2consts.Version)
		return
	}
	proc := []proto.IProcess{
		app.ServerProcess.Get(),
		app.SysInfoCacheProcess.Get(),
		app.AlerterProcess.Get(),
		app.KillerProcess.Get(),
	}
	go func() {
		time.Sleep(time.Second)
		app.StartStop.Get().DoAutostart()
	}()
	err := giter.GatherMapErr(
		func(p proto.IProcess) error { return p.Run() },
		proc...,
	)
	if err != nil {
		println("Err: " + err.Error())
	}
}
